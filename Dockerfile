FROM shisha/reactor-core

LABEL maintainer="grach"

# copy in code
ADD src/ /var/www/html/
ADD conf/nginx.conf /etc/nginx/
ADD conf/nginx/ /etc/nginx/conf.d/

ADD scripts/start.sh /start.sh
RUN chmod 755 /start.sh

EXPOSE 443 80

WORKDIR "/var/www/html"
CMD ["/start.sh"]
