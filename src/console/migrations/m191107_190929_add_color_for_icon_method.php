<?php

use common\modules\methods\models\MethodModel;
use yii\db\Migration;

/**
 * Class m191107_190929_add_color_for_icon_method
 */
class m191107_190929_add_color_for_icon_method extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(MethodModel::tableName(), 'background_for_icon', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191107_190929_add_color_for_icon_method cannot be reverted.\n";

        return false;
    }
}
