<?php

use yii\db\Migration;

/**
 * Class m190814_114636_add_table_files
 */
class m190814_114636_add_table_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'original_name' => $this->string(),
            'size' => $this->integer(),
            'created' => $this->integer(),
            'status' => $this->string(),
            'extension' => $this->string(),
            'path' => $this->string(),
            'type' => $this->string(),
            'model_id' => $this->integer(),
        ]);
        $this->createIndex('files_idx', 'files', ['name'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190814_114636_add_table_files cannot be reverted.\n";
    }
}
