<?php

use yii\db\Migration;

/**
 * Class m190611_121603_add_tables
 */
class m190611_121603_add_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('main_menu', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->string(),
            'status' => $this->string(),
            'link' => $this->string(),
            'icon' => $this->string(),
        ]);

        $this->createTable('theory', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'status' => $this->string(),
            'image' => $this->string(),
            'views' => $this->integer(),
            'created' => $this->integer(),
            'updated' => $this->integer(),
            'method_id' => $this->integer(),
        ]);

        $this->createTable('methods', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'description' => $this->text(),
            'status' => $this->string(),
            'image' => $this->string(),
            'views' => $this->integer(),
            'created' => $this->integer(),
            'updated' => $this->integer(),
            'difficulty' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190611_121603_add_tables cannot be reverted.\n";

        return false;
    }
}
