<?php

use yii\db\Migration;

/**
 * Class m190728_074811_add_color_and_icon_name_for_method
 */
class m190728_074811_add_color_and_icon_name_for_method extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\methods\models\Methods::tableName(), 'icon_color', $this->string());
        $this->addColumn(\common\modules\methods\models\Methods::tableName(), 'icon_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190728_074811_add_color_and_icon_name_for_method cannot be reverted.\n";

        return false;
    }
}
