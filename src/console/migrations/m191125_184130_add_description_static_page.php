<?php

use common\modules\description\models\DescriptionStaticPage;
use yii\db\Migration;

/**
 * Class m191125_184130_add_description_static_page
 */
class m191125_184130_add_description_static_page extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('description_static_page', [
            'id' => $this->primaryKey(),
            'page_name' => $this->string(),
            'text' => $this->text()
        ]);
        $this->insert(DescriptionStaticPage::tableName(), [
            'page_name' => DescriptionStaticPage::PAGE_METHODS
        ]);
        $this->insert(DescriptionStaticPage::tableName(), [
            'page_name' => DescriptionStaticPage::PAGE_TYPES
        ]);
        $this->insert(DescriptionStaticPage::tableName(), [
            'page_name' => DescriptionStaticPage::PAGE_REVIEWS
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191125_184130_add_description_static_page cannot be reverted.\n";
    }
}
