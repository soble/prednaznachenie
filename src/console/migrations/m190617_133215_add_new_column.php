<?php

use yii\db\Migration;

/**
 * Class m190617_133215_add_new_column
 */
class m190617_133215_add_new_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\theory\models\Theory::tableName(), 'date_publish', $this->integer());
        $this->addColumn(\common\modules\methods\models\Methods::tableName(), 'date_publish', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190617_133215_add_new_column cannot be reverted.\n";

        return false;
    }
}
