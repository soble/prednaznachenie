<?php

use yii\db\Migration;

/**
 * Class m190804_121100_add_rating_for_article
 */
class m190804_121100_add_rating_for_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'model_id' => $this->string(),
            'value' => $this->integer(),
            'created' => $this->integer(),
            'status' => $this->string(),
            'ip' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190804_121100_add_rating_for_article cannot be reverted.\n";

        return false;
    }
}
