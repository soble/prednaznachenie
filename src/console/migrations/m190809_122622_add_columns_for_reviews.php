<?php

use yii\db\Migration;

/**
 * Class m190809_122622_add_columns_for_reviews
 */
class m190809_122622_add_columns_for_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\reviews\models\Reviews::tableName(), 'error_message', $this->text());
        $this->addColumn(\common\modules\reviews\models\Reviews::tableName(), 'link_to_error', $this->text());
        $this->addColumn(\common\modules\reviews\models\Reviews::tableName(), 'age', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190809_122622_add_columns_for_reviews cannot be reverted.\n";

        return false;
    }
}
