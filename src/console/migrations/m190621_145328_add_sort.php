<?php

use yii\db\Migration;

/**
 * Class m190621_145328_add_sort
 */
class m190621_145328_add_sort extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\theory\models\Theory::tableName(), 'sort', $this->integer());
        $this->addColumn(\common\modules\methods\models\Methods::tableName(), 'sort', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190621_145328_add_sort cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190621_145328_add_sort cannot be reverted.\n";

        return false;
    }
    */
}
