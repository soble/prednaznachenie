<?php

use common\modules\methods\models\MethodModel;
use yii\db\Migration;

/**
 * Class m191111_193410_add_background_color
 */
class m191111_193410_add_background_color extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(MethodModel::tableName(), 'border_color_for_icon', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191111_193410_add_background_color cannot be reverted.\n";

        return false;
    }
}
