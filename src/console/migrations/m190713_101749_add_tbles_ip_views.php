<?php

use yii\db\Migration;

/**
 * Class m190713_101749_add_tbles_ip_views
 */
class m190713_101749_add_tbles_ip_views extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('relations_views_to_ip', [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer(),
            'type_model' => $this->string(),
            'ip' => $this->string(),
            'date_view' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190713_101749_add_tbles_ip_views cannot be reverted.\n";

        return false;
    }
}
