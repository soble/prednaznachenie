<?php

use yii\db\Migration;

/**
 * Class m190619_125211_add_2_tables
 */
class m190619_125211_add_2_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->integer(),
            'status' => $this->string(),
        ]);
        $this->createTable('theory_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->integer(),
            'status' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190619_125211_add_2_tables cannot be reverted.\n";
    }
}
