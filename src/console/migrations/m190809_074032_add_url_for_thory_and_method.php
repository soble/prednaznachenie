<?php

use yii\db\Migration;

/**
 * Class m190809_074032_add_url_for_thory_and_method
 */
class m190809_074032_add_url_for_thory_and_method extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\theory\models\Theory::tableName(), 'url', $this->string());
        $this->addColumn(\common\modules\methods\models\Methods::tableName(), 'url', $this->string());

        $this->createIndex(
            'url_idx_theory',
            \common\modules\theory\models\Theory::tableName(),
            ['url'],
            true
        );
        $this->createIndex(
            'url_idx_method',
            \common\modules\methods\models\Methods::tableName(),
            ['url'],
            true
        );

        $this->addUrlForMethods();
        $this->addUrlForTheory();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190809_074032_add_url_for_thory_and_method cannot be reverted.\n";

        return false;
    }

    private function addUrlForTheory()
    {
        foreach (\common\modules\theory\models\Theory::find()->all() as $theory) {
            $theory->url = \yii\helpers\Inflector::slug($theory->name);
            $theory->save();
        }
    }

    private function addUrlForMethods()
    {
        foreach (\common\modules\methods\models\Methods::find()->all() as $theory) {
            $theory->url = \yii\helpers\Inflector::slug($theory->name);
            $theory->save();
        }
    }
}
