<?php

use yii\db\Migration;

/**
 * Class m190617_074734_add_user_for_admin
 */
class m190617_074734_add_user_for_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE user MODIFY status VARCHAR(255)');

        $this->insert(\common\models\User::tableName(), [
            'username' => 'alexander',
            'auth_key' => 'adm_alexander',
            'password_hash' => Yii::$app->security->generatePasswordHash('Pred12n78'),
            'email' => 'kaviom@yandex.ru',
            'status' => \common\models\MainModel::STATUS_ACTIVE,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190617_074734_add_user_for_admin cannot be reverted.\n";
    }
}
