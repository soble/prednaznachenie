<?php

use yii\db\Migration;

/**
 * Class m190714_094629_add_main_menu
 */
class m190714_094629_add_main_menu extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(\common\modules\menu\models\MainMenu::tableName(), [
            'name' => 'Типизация',
            'sort' => '2',
            'status' => \common\modules\menu\models\MainMenu::STATUS_ACTIVE,
            'link' => '/theory/frontend',
            'icon' => '<i class="fa fa-book"></i>',
        ]);
        $this->insert(\common\modules\menu\models\MainMenu::tableName(), [
            'name' => 'Методы',
            'sort' => '1',
            'status' => \common\modules\menu\models\MainMenu::STATUS_ACTIVE,
            'link' => '/methods/frontend',
            'icon' => '<i class="fa fa-bullseye"></i>',
        ]);
        $this->insert(\common\modules\menu\models\MainMenu::tableName(), [
            'name' => 'О проекте',
            'sort' => '3',
            'status' => \common\modules\menu\models\MainMenu::STATUS_ACTIVE,
            'link' => '/site/about',
            'icon' => '<i class="fa fa-info"></i>',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190714_094629_add_main_menu cannot be reverted.\n";

        return false;
    }
}
