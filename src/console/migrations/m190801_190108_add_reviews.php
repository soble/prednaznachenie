<?php

use yii\db\Migration;

/**
 * Class m190801_190108_add_reviews
 */
class m190801_190108_add_reviews extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reviews', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'type' => $this->string(),
            'description' => $this->text(),
            'created' => $this->integer(),
            'status' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190801_190108_add_reviews cannot be reverted.\n";

        return false;
    }
}
