<?php

use yii\db\Migration;

/**
 * Class m190712_133524_add_table_relations_tags_and_method
 */
class m190712_133524_add_table_relations_tags_and_method extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('relations_methods_to_tag', [
            'id' => $this->primaryKey(),
            'method_id' => $this->integer(),
            'tag_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190712_133524_add_table_relations_tags_and_method cannot be reverted.\n";

        return false;
    }
}
