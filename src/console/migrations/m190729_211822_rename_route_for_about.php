<?php

use yii\db\Migration;

/**
 * Class m190729_211822_rename_route_for_about
 */
class m190729_211822_rename_route_for_about extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update(\common\modules\menu\models\MenuModel::tableName(), [
            'link' => '/about'
        ], ['link' => '/site/about']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190729_211822_rename_route_for_about cannot be reverted.\n";

        return false;
    }
}
