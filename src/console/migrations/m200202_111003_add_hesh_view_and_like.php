<?php

use common\models\Rating;
use common\models\RelationsViewsToIp;
use common\modules\methods\models\Methods;
use yii\db\Migration;

/**
 * Class m200202_111003_add_hesh_view_and_like
 */
class m200202_111003_add_hesh_view_and_like extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(RelationsViewsToIp::tableName(), 'hash', $this->string());
        $this->addColumn(Rating::tableName(), 'hash', $this->string());

        $this->addColumn(RelationsViewsToIp::tableName(), 'user_agent', $this->string());
        $this->addColumn(Rating::tableName(), 'user_agent', $this->string());

        $this->update(Methods::tableName(), [
            'views' => 0
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200202_111003_add_hesh_view_and_like cannot be reverted.\n";
    }
}
