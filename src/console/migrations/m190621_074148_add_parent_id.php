<?php

use yii\db\Migration;

/**
 * Class m190621_074148_add_parent_id
 */
class m190621_074148_add_parent_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\modules\theory\models\Theory::tableName(), 'parent_id', $this->integer());
        $this->addColumn(\common\modules\tags\models\Tags::tableName(), 'parent_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190621_074148_add_parent_id cannot be reverted.\n";

        return false;
    }
}
