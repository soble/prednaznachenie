<?php

use common\modules\methods\models\Methods;
use yii\db\Migration;

/**
 * Class m201114_101939_add_not_null
 */
class m201114_101939_add_not_null extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("ALTER TABLE public.methods
            ALTER COLUMN views SET DEFAULT 0;");
        $this->execute("ALTER TABLE public.methods
                ALTER COLUMN views SET NOT NULL;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201114_101939_add_not_null cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201114_101939_add_not_null cannot be reverted.\n";

        return false;
    }
    */
}
