<?php

use common\modules\seo\models\SeoModel;
use common\modules\methods\models\MethodModel;
use common\modules\theory\models\TheoryModel;
use yii\db\Migration;

/**
 * Class m191121_185132_add_ceo
 */
class m191121_185132_add_ceo extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('seo_for_static_page', [
            'id' => $this->primaryKey(),
            'page_name' => $this->string(),
            'created' => $this->integer(),
            'title' => $this->string(),
            'keywords' => $this->text(),
            'description' => $this->text(),
        ]);
        $this->addColumn(MethodModel::tableName(), 'seo_title', $this->string());
        $this->addColumn(MethodModel::tableName(), 'seo_keywords', $this->text());
        $this->addColumn(MethodModel::tableName(), 'seo_description', $this->text());

        $this->addColumn(TheoryModel::tableName(), 'seo_title', $this->string());
        $this->addColumn(TheoryModel::tableName(), 'seo_keywords', $this->text());
        $this->addColumn(TheoryModel::tableName(), 'seo_description', $this->text());


        $this->insert(SeoModel::tableName(), [
            'page_name' => SeoModel::PAGE_ABOUT,
            'created' => time()
        ]);
        $this->insert(SeoModel::tableName(), [
            'page_name' => SeoModel::PAGE_TYPES,
            'created' => time()
        ]);
        $this->insert(SeoModel::tableName(), [
            'page_name' => SeoModel::PAGE_METHODS,
            'created' => time()
        ]);
        $this->insert(SeoModel::tableName(), [
            'page_name' => SeoModel::PAGE_REVIEWS,
            'created' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191121_185132_add_ceo cannot be reverted.\n";

    }
}
