<?php

/* @var $this yii\web\View */

/* @var $type string */
/* @var $modelId string */
/* @var $urlForCreate string */
/* @var $urlForDelete string */

/* @var $files Files[] */

use yii\widgets\ActiveForm,
    \common\models\Files,
    \yii\helpers\Url;
use yii\bootstrap\Modal;

$model = new Files();
?>

<div class="row">
    <div class="col-md-12">
        <h3>
            Файлы
        </h3>
        <?php $form = ActiveForm::begin([
            'action' => $urlForCreate,
            'method' => 'POST'
        ]); ?>
        <?= $form->field($model, 'type')->hiddenInput(['value' => $type])->label(false) ?>
        <?= $form->field($model, 'model_id')->hiddenInput(['value' => $modelId])->label(false) ?>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?= $form->field($model, 'imageFile')->fileInput() ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <button class="btn btn btn-primary create-folder modal-close" type="submit">
                Загрузить
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Изображение</th>
                <th>Ссылка</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($files as $file) : ?>
            <tr>
                <td>
                    <img style="width: 150px" alt="<?= $file->name ?>" src="<?= $file->getUrl() ?>">
                </td>
                <td>
                    <?= $file->getUrl() ?>
                </td>
                <td>
                    <?php Modal::begin([
                        'header' => '<h2>Удалить фаил?</h2>',
                        'toggleButton' => [
                            'label' => '<i class="fa fa-trash"></i>',
                            'class' => 'btn btn-danger btn-sm'
                        ],
                        'size' => 'modal-md',
                        'options' => [
                            'tabindex' => false
                        ]
                    ]); ?>
                    <a class="btn btn-danger btn-sm" href="<?= Url::to([$urlForDelete, 'id' => $file->id]) ?>">
                        Удалить
                    </a>
                    <?php Modal::end() ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
