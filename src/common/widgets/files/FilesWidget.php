<?php

namespace common\widgets\files;

use common\models\Files;
use \yii\bootstrap\Widget;

class FilesWidget extends Widget
{
    public $type;
    public $modelId;
    public $urlForCreate;
    public $urlForDelete;

    public function run()
    {
        $files = Files::findAll(['type' => $this->type, 'model_id' => $this->modelId]);

        return $this->render('index', [
            'type' => $this->type,
            'modelId' => $this->modelId,
            'urlForCreate' => $this->urlForCreate,
            'urlForDelete' => $this->urlForDelete,
            'files' => $files
        ]);
    }
}
