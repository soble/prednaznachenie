<?php

use common\models\MainModel;
use yii\widgets\ActiveForm;

/** @var $model MainModel */
/** @var $url */
?>
<div class="row" style="margin-top: 20px">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => [$url, 'id' => $model->id],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder pull-right" type="submit">
                Сохранить
            </button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'seo_title')->textarea(['autofocus' => true, 'rows' => '1']) ?>
            <?= $form->field($model, 'seo_description')->textarea(['rows' => '7']) ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>

