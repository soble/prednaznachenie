<?php

use common\modules\seo\models\SeoModel;
use yii\widgets\ActiveForm;

/** @var $model SeoModel */
?>
<div class="row" style="margin-top: 20px">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/seo/seo/create-or-update', 'pageName' => $model->page_name],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder pull-right" type="submit">
                Сохранить
            </button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'title')->textarea(['autofocus' => true, 'rows' => '1']) ?>
            <?= $form->field($model, 'description')->textarea(['rows' => '7']) ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>

