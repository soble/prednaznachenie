<?php

namespace common\widgets\seo;

use common\modules\seo\models\SeoModel;
use \yii\bootstrap\Widget;

class SeoWidget extends Widget
{
    public $pageName;
    public $isStaticPage = true;
    public $model;
    public $url;

    public function run()
    {
        if ($this->isStaticPage) {
            return $this->getStaticPageForm();
        }
        if (!$this->isStaticPage) {
            return $this->getDynamicPageForm();
        }
    }

    public function getStaticPageForm()
    {
        $model = SeoModel::getModelByPageName($this->pageName);
        if ($model) {
            $response = $this->render('staticPage', [
                'model' => $model,
            ]);
        } else {
            $response = 'Добавьте запись для этой страници в таблицу  СЕО';
        }

        return $response;
    }

    public function getDynamicPageForm()
    {
        return $this->render('dynamicPage', [
            'model' => $this->model,
            'url' => $this->url,
        ]);
    }

    /**
     * @param string $pageName
     * @param string $defaultTitle
     * @throws \Exception
     */
    public static function registerStaticPageMetaTags($pageName, $defaultTitle)
    {
        $model = SeoModel::getModelByPageName($pageName);
        if (!$model) {
            throw new \Exception('Не найдена СЕО запись для страницы ' . $pageName);
        }

        \Yii::$app->controller->view->registerMetaTag([
            'name' => 'description',
            'content' => $model->description
        ], $pageName);
        \Yii::$app->controller->view->title = $model->title ? $model->title : $defaultTitle;
    }

    /**
     * @param $model
     * @throws \Exception
     */
    public static function registerDynamicPageMetaTags($model)
    {
        \Yii::$app->controller->view->registerMetaTag([
            'name' => 'description',
            'content' => $model->seo_description
        ], $model->id . '_dynamic_page' . time());

        \Yii::$app->controller->view->title = $model->seo_title ? $model->seo_title : $model->name;
    }
}
