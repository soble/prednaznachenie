<?php

namespace common\widgets\tree;

use \yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;

/**
 * Class TreeWidget
 * @package common\widgets\tree
 * @var array urlParams
 * @var string url
 * @var object[] items
 */
class TreeWidget extends Widget
{
    /** @var $items object[] */
    public $items;

    /** @var $url string */
    public $url;

    /** @var $urlParams array */
    public $urlParams = [];

    /** @var $forTags bool */
    public $forTags = false;

    /** @var $forMainTheory bool */
    public $forMainTheory = false;

    /** @var $forTheory bool */
    public $forTheory = false;

    public function run()
    {
        TreeAsset::register($this->view);

        if ($this->forTheory) {
            return $this->treeDetailTheoryFrontend();
        }
        if ($this->forMainTheory) {
            return $this->mainTheoryFrontend();
        }

        return $this->forTags ? $this->tagsList() : $this->treeHtml();
    }

    public function treeHtml()
    {
        $html = '<ul class="tree" style="padding: 0 5px">';
        foreach ($this->items as $item) {
            $tagDown = '<a class="pull-right show-children-items" href="#"><i class="fa fa-plus-square-o"></i></a>';

            $active = '';
            if ($item->isActive) {
                $active = 'active';
                $tagDown = '<a class="pull-right show-children-items" href="#"><i class="fa fa-minus-square-o"></i></a>';
            }

            $html .= '<li class="' . $active . '"> <a href="' . $this->getUrl($item) . '">' . $item->name . '</a>';

            if ($item->children) {
                $html .= $tagDown
                    . $this->childrenHtml($item->children);
            }
            $html .= '</li>';
        }

        return $html . '</ul>';
    }

    public function tagsList()
    {
        $tagsHtml = '';
        foreach ($this->items as $currentCategory => $item) {
            $active = $item->isActive ? 'active' : '';
            $tagsHtml .= '<li class="' . $active . ' parent">
                    <a href="' . $this->getUrl($item) . '">
                        <i class="fa fa-tags"></i> <span class="nav-label">' . $item->name . '</span></a></li>';

            if ($item->children) {
                foreach ($item->children as $children) {
                    $childrenActive = $children->isActive ? 'active' : '';
                    $tagsHtml .= '<li style="padding-left: 15px" class="' . $childrenActive
                        . '"><a href="' . $this->getUrl($children) . '">'
                        . '<i class="fa fa-tag"></i><span class="nav-label">' . $children->name . '</span></a></li>';;
                }
            }
        }

        return $tagsHtml . '<li class="parent" style="margin-bottom: 40px"></li>';
    }

    public function mainTheoryFrontend()
    {
        $menuHtml = '';
        foreach ($this->items as $currentCategory => $item) {
            $active = $item->isActive ? 'active' : '';
            $menuHtml .= '<li class="' . $active . '">
                    <a href="' . $this->getUrl($item) . '">
                        <span class="nav-label">' . $item->name . '</span></a></li>';
        }

        return $menuHtml;
    }

    public function treeDetailTheoryFrontend()
    {
        $menuHtml = '';
        foreach ($this->items as $currentCategory => $item) {
            $active = $item->isActive ? 'active' : '';
            $menuHtml .= '<li class="' . $active . '">
                    <a href="' . $this->getUrl($item) . '">
                        <span class="nav-label">' . $item->name . '</span></a></li>';

            if ($item->children) {
                foreach ($item->children as $children) {
                    $childrenActive = $children->isActive ? 'active' : '';
                    $menuHtml .= '<li style="padding-left: 15px" class="' . $childrenActive
                        . '"><a href="' . $this->getUrl($children) . '">'
                        . '<span class="nav-label">' . $children->name . '</span></a></li>';;
                }
            }
        }

        return $menuHtml;
    }

    private function childrenHtml($childrenItems)
    {

        $html = '<ul class="tree child-item">';
        foreach ($childrenItems as $item) {
            $active = '';
            $tagDown = '<a class="pull-right show-children-items" href="#"><i class="fa fa-plus-square-o"></i></a>';
            if ($item->isActive) {
                $active = 'active';
                $tagDown = '<a class="pull-right show-children-items" href="#"><i class="fa fa-minus-square-o"></i></a>';
            }
            $html .= '<li class="' . $active . '"> <a href="' . $this->getUrl($item) . '">' . $item->name . '</a>';
            if ($item->children) {
                $html .= $tagDown
                    . $this->childrenHtml($item->children);
            }
            $html .= '</li>';
        }

        return $html . '</ul>';
    }

    private function getUrl($item)
    {
        $params = [];
        foreach ($this->urlParams as $paramName => $attribute) {
            $params[$paramName] = isset($item->$attribute) ? $item->$attribute : $attribute;
        }

        return Url::to(array_merge([$this->url], $params));
    }
}
