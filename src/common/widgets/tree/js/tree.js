$(document).ready(function () {
    $(document).on('click', '.show-children-items', showChildCategories);
});

function showChildCategories(e) {
    e.preventDefault();
    let element = $(this).siblings('ul');
    let li = $(this).parents('li');
    if (element.is(':visible')) {
        element.slideUp();
        li.removeClass('active');
        $(this).find('i').addClass('fa-plus-square-o');
        $(this).find('i').removeClass('fa-minus-square-o');
    } else {
        element.slideDown();
        li.addClass('active');
        $(this).find('i').removeClass('fa-plus-square-o');
        $(this).find('i').addClass('fa-minus-square-o');
    }
}

