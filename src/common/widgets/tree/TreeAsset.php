<?php

namespace common\widgets\tree;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class TreeAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets/tree';
    public $css = [
        'css/tree.css'
    ];
    public $js = [
        'js/tree.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
