<?php

namespace common\traits;

use yii\base\InvalidRouteException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

trait ErrorTrait
{
    /**
     * @param $exception \Throwable
     * @param $action
     * @return false|string
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function buildError($exception, $action)
    {
        if ($exception instanceof InvalidRouteException) {
            throw new NotFoundHttpException();
        }

        if (isset($exception->statusCode) && $exception->statusCode == 404) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        if ($exception->getCode() == self::ERROR_CODE_FOR_ADMIN) {
            self::adminError($exception, $action);
            \Yii::$app->session->removeFlash('error');
        }
        if ($exception->getCode() == self::EMPTY_CODE) {
            self::adminError($exception, $action);
            return self::userErrorNotify();
        }
        if ($exception->getCode() == self::PHP_ERROR_CODE) {
            self::adminError($exception, $action);
            return self::userErrorNotify();
        }
        if ($exception->getCode() == self::ERROR_CODE_FOR_USER_AND_ADMIN) {
            self::adminError($exception, $action);
            return self::userErrorNotify();
        }
        if ($exception->getCode() == self::NOTIFY_USER_AND_ADMIN) {
            self::adminError($exception, $action);
            return self::userNotify($exception);
        }
        if ($exception->getCode() == self::NOTIFY_CODE_FOR_USER) {
            return self::userNotify($exception);
        }
        self::adminError($exception, $action);

        return self::userErrorNotify();
    }

    /**
     * @param $exception \Throwable
     * @param $action
     */
    private function adminError($exception, $action)
    {
        $codError = '<p> Code error - ' . $exception->getCode() . '</p>';
        $controller = '<p> controller - ' . \Yii::$app->controller->id . '</p>';
        $action = '<p> action - ' . $action . '</p>';
        $line = '<p> line - ' . $exception->getLine() . '</p>';
        $error = '<p> error - ' . $exception->getMessage() . '</p>';
        $trace = '<p> trace - ' . $exception->getTraceAsString() . '</p>';

//        Yii::$app->mailer->compose()
//            ->setTo('shishatskiy@mjr.ru')
//            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
//            ->setSubject('err')
//            ->setTextBody($codError . $controller . $action . $line . $error . $trace)
//            ->send();
    }

    private function userErrorNotify()
    {
        $statuses = ['error', 'success'];
        $isSetFlashMessage = false;
        foreach ($statuses as $status) {
            if (\Yii::$app->session->hasFlash($status)) {
                if (\Yii::$app->request->isAjax) {
                    $this->ajaxMessage = \Yii::$app->session->getFlash($status);
                    $this->ajaxSuccess = $status == 'error' ? false : true;
                    \Yii::$app->session->removeFlash($status);
                }
                $isSetFlashMessage = true;
            }
        }
        if (!$isSetFlashMessage) {
            throw new HttpException(500,
                'Произошла ошибка. Мы уже решаем эту проблему. Попробуйте повторить действие через 2 минуты.');
        }
    }

    /**
     * @param $exception \Throwable
     * @return string
     * @throws HttpException
     */
    private function userNotify($exception)
    {
        if (\Yii::$app->session->hasFlash('error')) {
            $message = \Yii::$app->session->getFlash('error');
            \Yii::$app->session->removeFlash('error');
        } else {
            $message = $exception->getMessage();
        }
        if (!\Yii::$app->request->isAjax) {
            \Yii::$app->session->setFlash('error', $message);
            if (\Yii::$app->request->referrer) {
                return $this->redirect(\Yii::$app->request->referrer);
            } else {
                return $this->redirect('/');
            }
        } else {
            $jsonData = json_encode([
                'success' =>  false,
                'message' => $message
            ]);

            return $jsonData;
        }
    }
}
