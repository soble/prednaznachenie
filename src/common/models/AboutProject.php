<?php

namespace common\models;

/**
 * This is the model class for table "about_project".
 *
 * @property int $id
 * @property string $description
 */
class AboutProject extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
        ];
    }

    public static function saveData($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new \Exception('Модель не найдена saveData');
        }
        $model->load(\Yii::$app->request->post());
        $model->save();
    }
}
