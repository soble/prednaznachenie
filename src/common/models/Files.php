<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $name
 * @property string $original_name
 * @property int $size
 * @property int $created
 * @property string $status
 * @property string $extension
 * @property string $path
 * @property string $type
 * @property int $model_id
 */
class Files extends MainModel
{
    const TYPE_ABOUT = 'about';
    const TYPE_THEORY = 'theory';
    const TYPE_METHODS = 'methods';

    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['size', 'created', 'model_id'], 'integer'],
            [['name', 'original_name', 'extension', 'path', 'type', 'size', 'model_id'], 'required'],
            [['name', 'original_name', 'status', 'extension', 'path', 'type'], 'string', 'max' => 255],
            [['name'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'original_name' => 'Имя оригенала',
            'size' => 'Размер',
            'created' => 'Создано',
            'status' => 'Статус',
            'extension' => 'Расширение',
            'path' => 'Путь'
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }

    public static function deleteFile($id)
    {
        $file = self::findOne($id);
        if (!$file) {
            throw new NotFoundHttpException('Фаил не найден');
        }
        unlink(\Yii::$app->params['pathToSaveImage'] . 'files/'
            . $file->name . '.' . $file->extension);
        $file->delete();
    }

    public function getUrl()
    {
        return \Yii::$app->params['urlForBackImage'] . $this->path;
    }

    public function upload()
    {
        $result = false;
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if ($this->imageFile) {
            $this->imageFile->saveAs(\Yii::$app->params['pathToSaveImage']
                . 'files/'
                . $this->name
                . '.'
                . $this->imageFile->extension);
            $this->path = 'uploads/files/' . $this->name . '.' . $this->imageFile->extension;
            $this->original_name = $this->imageFile->baseName;
            $this->extension = $this->imageFile->extension;
            $this->size = $this->imageFile->size;
            $result = true;
            $this->save();
        }

        return $result;
    }
}
