<?php

namespace common\models;

use yii\helpers\Html;

class HtmlModel
{
    const SORT_BY_DATE = 'sortByDate';
    const SORT_BY_VIEWS = 'sortByViews';
    const SORT_BY_RATING = 'sortByRating';

    public static function sortButton($url, $size = 'sm')
    {
        $urlDate = array_merge(
            [$url],
            [self::SORT_BY_DATE => self::sortValue(self::SORT_BY_DATE)]);
        $urlViews = array_merge([$url],
            [self::SORT_BY_VIEWS => self::sortValue(self::SORT_BY_VIEWS)]);
        $urlRating = array_merge([$url],
            [self::SORT_BY_RATING => self::sortValue(self::SORT_BY_RATING)]);


        return Html::a('Опубликовано ' . self::getIcon(self::SORT_BY_DATE),
                $urlDate,
                ['class' => "btn btn-{$size} btn-default"])
            . ' '
            . Html::a('Рейтинг ' . self::getIcon(self::SORT_BY_RATING),
                $urlRating,
                ['class' => "btn btn-{$size} btn-default"])
            . ' '
            . Html::a('Просмотры ' . self::getIcon(self::SORT_BY_VIEWS),
                $urlViews,
                ['class' => "btn btn-{$size} btn-default"]);
    }

    private static function sortValue($sortParamName)
    {
        $params = [
            self::SORT_BY_DATE => 'ASC',
            self::SORT_BY_VIEWS => 'DESC',
            self::SORT_BY_RATING => 'DESC'
        ];
        foreach ($params as $paramName => $sortValue) {
            if (\Yii::$app->request->get($paramName)) {
                $sortValue = \Yii::$app->request->get($paramName) == 'ASC' ? 'DESC' : 'ASC';
                $params[$paramName] = $sortValue;
            }
        }

        return isset($params[$sortParamName]) ? $params[$sortParamName] : null;
    }

    private static function getIcon($sortParamName)
    {
        return self::sortValue($sortParamName) == 'ASC'
            ? '<i class="fa fa-sort-amount-desc"></i>'
            : '<i class="fa fa-sort-amount-asc"></i>';
    }
}
