<?php

namespace common\models;

use frontend\controllers\MainController;
use yii\behaviors\TimestampBehavior,
    yii\db\ActiveRecord,
    \himiklab\yii2\recaptcha\ReCaptchaValidator3;

/**
 * This is the model class for table "rating".
 *
 * @property int $id
 * @property string $type
 * @property string $model_id
 * @property int $value
 * @property int $created
 * @property string $status
 * @property string $ip
 * @property string $hash
 * @property string $user_agent
 */
class Rating extends MainModel
{
    const TYPE_METHOD = 'method';
    const TYPE_THEORY = 'theory';

    public $captcha;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rating';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value', 'created'], 'integer'],
            [['type', 'model_id', 'status', 'ip', 'hash', 'user_agent'], 'string', 'max' => 255],
            [['type', 'model_id', 'ip', 'hash', 'user_agent'], 'required'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['value'], 'required', 'message' => 'Необходимо поставить оценку'],
//            [['captcha'], ReCaptchaValidator3::class,
//                'threshold' => 0.1,
//                'action' => 'rating',
//                'message' => \Yii::$app->params['errorMessageCaptcha']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'model_id' => 'Model ID',
            'value' => 'Оценка',
            'created' => 'Created',
            'status' => 'Status',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_DEFAULT => array_merge($this->attributes(), ['captcha']),
        ]);
    }

    public static function issetRating($type, $modelId)
    {
        $userAgent = \Yii::$app->request->userAgent . \Yii::$app->request->remoteIP;
        $hash = md5($userAgent);

        return self::findOne(['hash' => $hash, 'type' => $type, 'model_id' => $modelId]) ? true : false;
    }

    public static function saveRating($postData, $type, $modelId)
    {
        $userAgent = \Yii::$app->request->userAgent . \Yii::$app->request->remoteIP;
        $hash = md5($userAgent);
        $model = new self();
        $model->load($postData);
        if (self::findOne(['hash' => $hash, 'type' => $type, 'model_id' => $modelId])) {
            throw new \Exception('Вы уже оценили этот метод', MainController::NOTIFY_CODE_FOR_USER);
        }
        $model->type = $type;
        $model->model_id = $modelId;
        $model->user_agent = \Yii::$app->request->userAgent;
        $model->ip = \Yii::$app->request->remoteIP;
        $model->hash = $hash;
        $model->save();
    }
}
