<?php

namespace common\models;

/**
 * This is the model class for table "relations_views_to_ip".
 *
 * @property int $id
 * @property int $model_id
 * @property string $type_model
 * @property string $ip
 * @property string $hash
 * @property string $user_agent
 * @property int $date_view
 */
class RelationsViewsToIp extends \yii\db\ActiveRecord
{
    const TYPE_METHOD = 'method';
    const TYPE_THEORY = 'theory';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'relations_views_to_ip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['model_id', 'date_view'], 'integer'],
            [['model_id', 'type_model', 'ip', 'hash', 'user_agent'], 'required'],
            [['date_view'], 'default', 'value' => time()],
            [['type_model', 'ip', 'hash', 'user_agent'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'model_id' => 'Model ID',
            'type_model' => 'Type Model',
            'ip' => 'Ip',
            'date_view' => 'Date View',
        ];
    }

    public static function getCountViews($type, $modelId)
    {
        return self::find()->where(['type_model' => $type, 'model_id' => $modelId])->count();
    }

    /**
     * @param $type
     * @param $modelId
     * @return int|string
     * @throws \yii\base\Exception
     */
    public static function setView($type, $modelId)
    {
        $hash = self::getHashCurrentUser();
        $currentModel = self::findOne(['type_model' => $type, 'model_id' => $modelId, 'hash' => $hash]);
        if (!$currentModel) {
            $model = new self();
            $model->type_model = $type;
            $model->model_id = $modelId;
            $model->ip = \Yii::$app->request->remoteIP;
            $model->hash = $hash;
            $model->user_agent = \Yii::$app->request->userAgent;
            $model->save();
        }

        return self::getCountViews($type, $modelId);
    }

    public static function getHashCurrentUser(): string
    {
        return md5(\Yii::$app->request->userAgent . \Yii::$app->request->remoteIP);
    }
}
