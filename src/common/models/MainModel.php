<?php

namespace common\models;

use backend\controllers\MainController;
use yii\helpers\Html;
use \yii\db\ActiveRecord;
use yii\helpers\Inflector;

/**
 * Class MainModel
 * @package common\models
 * @property string status
 */
class MainModel extends ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_NEW = 'new';
    const STATUS_DELETED = 'deleted';
    const STATUS_INACTIVE = 'inactive';

    public static function statusForSelect()
    {
        return [
            self::STATUS_NEW => self::STATUS_NEW,
            self::STATUS_ACTIVE => self::STATUS_ACTIVE,
            self::STATUS_DELETED => self::STATUS_DELETED,
        ];
    }

    public function statusHtml()
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                $class = 'warning';
                break;
            case self::STATUS_DELETED:
                $class = 'danger';
                break;
            case self::STATUS_ACTIVE:
                $class = 'primary';
                break;
            default:
                $class = 'success';
        }

        return Html::tag(
            'span',
            Html::encode(\Yii::t('app', $this->status)),
            ['class' => 'label label-' . $class]
        );
    }

    public function afterValidate()
    {
        parent::afterValidate();
        if ($this->hasErrors()) {
            $messagesTxt = '';
            $row = 1;
            foreach ($this->getFirstErrors() as $attribute => $message) {
                $messagesTxt = $messagesTxt . '<br />' . $row . ') ' . $message;
                $row++;
            }
            throw new \Exception($messagesTxt, MainController::NOTIFY_CODE_FOR_USER);
        }
    }

    public function getNameForUrl()
    {
        $nameUrl = '';
        if (isset($this->url)) {
            $nameUrl = $this->url;
        }

        return $nameUrl;
    }

    public function getViews()
    {
        $views = $this->views / 1000;

        return $views >= 1 ? round($views, 1) . 'K' : $this->views;
    }
}
