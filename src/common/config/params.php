<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'pathToSaveImage' => '/var/www/html/frontend/web/uploads/',
    'errorMessageCaptcha' => 'Ваше сообщение не прошло проверку на робота. 
    Пожалуйста обновите страницу и попробуйте отправить сообщение ещё раз'
];
