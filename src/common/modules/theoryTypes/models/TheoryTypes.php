<?php

namespace common\modules\theoryTypes\models;

use common\models\MainModel;

/**
 * This is the model class for table "theory_types".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $sort
 */
class TheoryTypes extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'theory_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'string', 'max' => 255],
            [['sort'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_NEW]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'status' => 'Статус',
            'sort' => 'Сортировка',
        ];
    }
}
