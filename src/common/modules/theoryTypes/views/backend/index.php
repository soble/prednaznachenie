<?php

/* @var $this yii\web\View */

use \yii\grid\GridView;
use \common\modules\theoryTypes\models\TheoryTypesModel;
use \yii\helpers\Html;

$this->title = 'Типы типизаций';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Типы типизаций
                    <small class="m-l-sm">Список типизаций</small>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <?= $this->render('_createModal')?>
                <?= GridView::widget([
                    'dataProvider' => TheoryTypesModel::getMenuDataProvider(),
                    'columns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Наименование',
                            'content' => function ($data) {
                                return Html::a($data->name, ['/theory-types/backend/view', 'id' => $data->id]);
                            }
                        ],
                        [
                            'attribute' => 'sort',
                            'label' => 'Сортировка',
                            'content' => function ($data) {
                                return $data->sort;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'content' => function ($data) {
                                return $data->statusHtml();
                            }
                        ],
                        [
                            'label' => '#',
                            'content' => function ($data) {
                                return Html::a(
                                        '<i class="fa fa-trash"></i>',
                                        ['/theory-types/backend/delete', 'id' => $data->id]
                                );
                            }
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
