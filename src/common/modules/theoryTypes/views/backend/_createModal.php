<?php

use \common\modules\theoryTypes\models\TheoryTypesModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;

$model = new TheoryTypesModel(['scenario' => TheoryTypesModel::SCENARIO_CREATE]);
?>
<?php Modal::begin([
    'header' => '<h2>Создание типа</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-plus"></i> Тип',
        'class' => 'btn btn btn-primary'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/theory-types/backend/create'],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
               Создать
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
