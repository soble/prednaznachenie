<?php

namespace common\modules\theoryTypes\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\theoryTypes\models\TheoryTypesModel;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_THEORY_TYPES);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $id = TheoryTypesModel::createModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = TheoryTypesModel::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        TheoryTypesModel::updateModel($id,\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionRelease($id)
    {
        TheoryTypesModel::release($id);
        \Yii::$app->session->setFlash('success', 'Тэг активирован');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        TheoryTypesModel::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
