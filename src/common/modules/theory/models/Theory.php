<?php

namespace common\modules\theory\models;

use common\models\MainModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "theory".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $image
 * @property int $views
 * @property int $created
 * @property int $updated
 * @property int $method_id
 * @property int $date_publish
 * @property int $parent_id
 * @property int $sort
 * @property string $url
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 */
class Theory extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'theory';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'seo_title', 'seo_keywords', 'seo_description'], 'string'],
            [['views', 'created', 'updated', 'method_id', 'date_publish', 'parent_id', 'sort'], 'integer'],
            [['name', 'status', 'image', 'url'], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_NEW]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'status' => 'Статус',
            'image' => 'Картинка',
            'views' => 'Просмотры',
            'created' => 'Создано',
            'updated' => 'Обновлено',
            'method_id' => 'Метод',
            'date_publish' => 'Дата публикации',
            'parent_id' => 'Родительская типизация',
            'sort' => 'Сортировка',
            'url' => 'Url для подставновки',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description'
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value' => time(),
            ],
        ];
    }
}
