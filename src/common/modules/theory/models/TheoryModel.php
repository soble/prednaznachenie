<?php

namespace common\modules\theory\models;

use common\models\RelationsViewsToIp;
use common\modules\theory\traits\Html;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class TheoryModel
 * @package common\modules\theory\models
 * @property UploadedFile $imageFile
 */
class TheoryModel extends Theory
{
    use Html;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_RELEASE = 'release';

    public $imageFile;
    public $button;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'created',
                'updated',
                'name',
                'status',
                'description',
                'url'
            ], 'required', 'on' => self::SCENARIO_RELEASE],
            [['name'], 'required', 'on' => self::SCENARIO_CREATE],
            [['button'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_RELEASE => $this->attributes()
        ]);
    }

    public static function createModel($postData, $currentTheory = null)
    {
        $model = new self();
        $model->load($postData);
        if ($currentTheory) {
            $model->parent_id = $currentTheory;
        }
        $model->save();

        return $model->id;
    }

    public static function updateModel($id, $postData)
    {
        $model = TheorySearch::getModel($id);
        $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
        if ($model->imageFile) {
            $model->upload();
        }
        $model->load($postData);
        if ($model->button == 'activated') {
            $model->status = self::STATUS_ACTIVE;
            $model->date_publish = time();
        }
        $model->scenario = $model->status == self::STATUS_ACTIVE
            ? self::SCENARIO_RELEASE
            : self::SCENARIO_DEFAULT;
        $model->save();

        return $model->id;
    }

    public static function release($id)
    {
        $model = TheorySearch::getModel($id);
        $model->scenario = self::SCENARIO_RELEASE;
        $model->status = self::STATUS_ACTIVE;
        $model->date_publish = time();
        $model->save();
    }

    public static function deleteModel($id)
    {
        $model = TheorySearch::getModel($id);
        $model->status = self::STATUS_DELETED;
        $model->save();
    }

    public static function deleteModelBase($id)
    {
        $model = TheorySearch::getModel($id);
        $model->delete();
    }

    public function tagsForSelectParent()
    {
        return ArrayHelper::map(self::find()->where(['<>', 'id', $this->id])->all(), 'id', 'name');
    }

    public function upload()
    {
        $uniqName = uniqid($this->id);
        if ($this->validate()) {
            $this->imageFile->saveAs(\Yii::$app->params['pathToSaveImage']
                . 'theory/'
                . $uniqName
                . '.'
                . $this->imageFile->extension);
            $this->image = 'uploads/theory/' . $uniqName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    public function setView()
    {
        $countViews = RelationsViewsToIp::setView(RelationsViewsToIp::TYPE_THEORY, $this->id);
        $this->views = $countViews;
        $this->save();
    }
}
