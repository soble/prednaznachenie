<?php

namespace common\modules\theory\models;

use common\models\HtmlModel;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class TheorySearch extends TheoryModel
{
    const PAGE = 20;

    public $datePublishBegin;
    public $datePublishEnd;
    public $methodsFilter;
    public $parentTheories;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['datePublishBegin', 'datePublishEnd'], 'string'],
            [['methodsFilter'], 'safe']
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'datePublishBegin' => 'Публикации начало',
            'datePublishEnd' => 'Публикации конец',
        ]);
    }

    public static function getModel($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Модель не найдена');
        }

        return $model;
    }

    public static function getModelByUrlName($name)
    {
        $model = self::findOne(['url' => $name]);
        if (!$model) {
            throw new NotFoundHttpException('Модель не найдена');
        }

        return $model;
    }

    public static function getMainTheories()
    {
        return self::find()->where(['status' => self::STATUS_ACTIVE])
            ->andWhere(['is', 'parent_id', null])
            ->orderBy('sort')
            ->indexBy('id')->all();
    }

    public static function getChildrenTheories($parentId, $theories = null)
    {
        if (!$theories) {
            $theories = self::find()->all();
        }
        $childrenTags = [];
        foreach ($theories as $theory) {
            if ($theory->parent_id == $parentId) {
                $theory->children = self::getChildrenTheories($theory->id, $theories);
                $childrenTags[$theory->id] = $theory;
            }
        }

        return $childrenTags;
    }

    public static function getParentTheories($parentId, $theories = null)
    {
        if (!$theories) {
            $theories = self::find()->all();
        }
        $parentTheories = [];
        foreach ($theories as $theory) {
            if ($theory->id == $parentId) {
                $parent = [];
                if ($theory->parent_id) {
                    $parent = self::getParentTheories($theory->parent_id, $theories);
                }
                $parentTheories = array_merge($parent, [$theory->name => $theory]);
            }
        }

        return $parentTheories;
    }

    public static function getTheoriesForSearch($searchText)
    {
        $searchTheories = self::find()
            ->where([
                'status' => self::STATUS_ACTIVE,
            ])
            ->andWhere(['not', ['parent_id' => null]])
            ->andWhere(['or', ['like', 'name', $searchText], ['like', 'description', $searchText]])
            ->all();
        foreach ($searchTheories as $searchTheory) {
            $searchTheory->parentTheories = self::getParentTheories($searchTheory->parent_id);
        }

        return $searchTheories;
    }

    public function getTheoryDataProvider()
    {
        $currentTheory = \Yii::$app->request->get('currentTheory');
        $allTheories = [];
        if ($currentTheory) {
            $childrenTags = self::getChildrenTheories($currentTheory);
            $allTheories = array_merge([$currentTheory], array_keys($childrenTags));
        }

        $query = self::find()
            ->filterWhere([
                'status' => $this->status,
                'method_id' => $this->methodsFilter,
                'id' => $allTheories
            ])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['between', 'date_publish', $this->datePublishBegin, $this->datePublishEnd])
            ->andFilterWhere(['>=', 'views', $this->views])
        ->orderBy('sort');

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
            'sort' => [
                'defaultOrder' => [
                    'date_publish' => SORT_DESC,
                ]
            ],
        ]);
    }

    public function getTheoriesFrontend($parentId = null)
    {
        $theoriesId = $parentId
            ? array_keys(self::getChildrenTheories($parentId))
            : array_keys(self::getMainTheories());

        $query = self::find()
            ->where([
                'theory.status' => self::STATUS_ACTIVE
            ])
            ->select([
                'theory.*',
            ])
            ->andWhere(['id' => $theoriesId])
            ->andFilterWhere(['like', 'name', $this->name])
            ->orderBy('sort');
        if (\Yii::$app->request->get(HtmlModel::SORT_BY_DATE)) {
            $query->addOrderBy('date_publish ' . \Yii::$app->request->get(HtmlModel::SORT_BY_DATE));
        }
        if (\Yii::$app->request->get(HtmlModel::SORT_BY_VIEWS)) {
            $query->addOrderBy('views ' . \Yii::$app->request->get(HtmlModel::SORT_BY_VIEWS));
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
        ]);
    }
}
