<?php

namespace common\modules\theory\controllers;

use common\modules\seo\models\SeoModel;
use common\modules\theory\models\TheorySearch;
use common\widgets\seo\SeoWidget;
use common\widgets\tree\TreeWidget;
use \frontend\controllers\MainController;
use yii\helpers\Inflector;

/**
 * Default controller for the `menu` module
 */
class FrontendController extends MainController
{
    const NAME_MENU = '/theory/frontend';

    /** @var $searchModel TheorySearch */
    public $searchModel = null;

    /** @var $childTheories TheorySearch[] */
    public $childTheories;

    /** @var $searchModel TheorySearch */
    public $parentTheory;

    public function init()
    {
        $model = new TheorySearch();
        $model->load(\Yii::$app->request->get());

        $this->searchModel = $model;

        if (\Yii::$app->request->get('parentTheoryName')) {
            $this->parentTheory = TheorySearch::getModelByUrlName(\Yii::$app->request->get('parentTheoryName'));
            $this->leftMenuTitle = '<span style="color: #f8ac59; font-size: 16px;">'
                . $this->parentTheory->name . '</span>';
            $this->childTheories = TheorySearch::childTheories(
                $this->parentTheory->id,
                null,
                \Yii::$app->request->get('theoryName'));
            $this->leftMenuItems = TreeWidget::widget([
                'items' => $this->childTheories,
                'url' => '/theory/frontend/view',
                'urlParams' => [
                    'parentTheoryName' => $this->parentTheory->getNameForUrl(),
                    'theoryName' => 'url',
                ],
                'forTheory' => true
            ]);
        } else {
            $this->leftMenuItems = TreeWidget::widget([
                'items' => TheorySearch::getMainTheories(),
                'url' => '/theory/frontend/child-theories',
                'urlParams' => ['parentTheory' => 'id'],
                'forMainTheory' => true
            ]);
        }
        parent::init();
    }

    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', self::NAME_MENU);
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        SeoWidget::registerStaticPageMetaTags(SeoModel::PAGE_TYPES, 'Типизация');
        return $this->render('index', [
            'dataProvider' => $this->searchModel->getTheoriesFrontend(),
            'searchModel' => $this->searchModel,
            'isMainTheory' => true
        ]);
    }

    public function actionChildTheories($parentTheory)
    {
        $parentTheory = TheorySearch::getModel($parentTheory);
        $model = current($this->searchModel->getTheoriesFrontend($parentTheory->id)->getModels());
        if (!$model) {
            \Yii::$app->session->setFlash('warning', 'Для этого раздела отсутствуют типизации');

            return $this->redirect(['/theory/frontend']);
        }
        $model->setView();

        return $this->redirect(['view',
            'parentTheoryName' => $parentTheory->getNameForUrl(),
            'theoryName' => $model->getNameForUrl()]);
    }

    public function actionTheoryBySearch($id)
    {
        $model = TheorySearch::getModel($id);
        $parentTheory = TheorySearch::getModel($model->parent_id);

        return $this->redirect(['view',
            'parentTheoryName' => $parentTheory->getNameForUrl(),
            'theoryName' => $model->getNameForUrl()]);
    }

    public function actionView($theoryName)
    {
        $model = TheorySearch::getModelByUrlName($theoryName);
        SeoWidget::registerDynamicPageMetaTags($model);

        return $this->render('view', [
            'model' => $model,
            'childTheories' => $this->childTheories,
            'parentTheory' => $this->parentTheory
        ]);
    }
}
