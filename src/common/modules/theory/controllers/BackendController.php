<?php

namespace common\modules\theory\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\theory\models\TheoryModel;
use common\modules\theory\models\TheorySearch;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_THEORY);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new TheorySearch();
        $model->load(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $model->getTheoryDataProvider(),
            'searchModel' => $model
        ]);
    }

    public function actionCreate($currentTheory = null)
    {
        $id = TheoryModel::createModel(\Yii::$app->request->post(), $currentTheory);
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = TheorySearch::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        TheoryModel::updateModel($id,\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        TheoryModel::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }

    public function actionDeleteBase($id)
    {
        TheoryModel::deleteModelBase($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
