<?php

namespace common\modules\theory\traits;

use yii\helpers\Url;

trait Html
{

    public $children;
    public $isActive;

    public static function childTheories($parentId = null, $theories = null, $currentTheoryUrl = null)
    {
        if (!$theories) {
            $theories = self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy('sort')->all();
        }

        $theoriesSort = [];

        foreach ($theories as $theory) {
            if ($theory->parent_id == $parentId) {
                $theory->children = self::childTheories($theory->id, $theories, $currentTheoryUrl);
                $theory->isActive = $currentTheoryUrl == $theory->url;
                $theoriesSort[$theory->id] = $theory;
            }
        }

        return $theoriesSort;
    }

    /**
     * @param null $theories
     * @param null $parentId
     * @param null $getName
     * @return self[]
     */
    public static function theoryTree($theories = null, $parentId = null, $getName = null)
    {
        if (!$theories) {
            $theories = self::find()->orderBy('sort')->all();
        }
        $getName = $getName ? $getName : 'currentTheory';

        $theoriesSort = [];
        $currentTheoryId = null;
        if (!empty($theories)) {
            $currentTheoryId = \Yii::$app->request->get($getName)
                ? \Yii::$app->request->get($getName)
                : current($theories)->id;
        }

        foreach ($theories as $theory) {
            if (!$parentId) {
                if (empty($theory->parent_id)) {
                    $theory->children = self::theoryTree($theories, $theory->id, $getName);
                    $isChildrenActive = array_key_exists('isActive', $theory->children)
                        ? $theory->children['isActive']
                        : false;
                    unset($theory->children['isActive']);
                    $isActive = $currentTheoryId == $theory->id ? true : $isChildrenActive;
                    $theory->isActive = $isActive;
                    $theoriesSort[$theory->id] = $theory;
                }
            } else {
                if ($theory->parent_id == $parentId) {
                    $theory->children = self::theoryTree($theories, $theory->id, $getName);
                    $isChildrenActive = array_key_exists('isActive', $theory->children)
                        ? $theory->children['isActive']
                        : false;
                    unset($theory->children['isActive']);
                    $isActive = $currentTheoryId == $theory->id ? true : $isChildrenActive;
                    $theory->isActive = $isActive;
                    $theoriesSort[$theory->id] = $theory;
                    $theoriesSort['isActive'] = isset($theoriesSort['isActive']) && $theoriesSort['isActive'] == true
                        ? true
                        : $isActive;
                }
            }
        }

        return $theoriesSort;
    }

    /**
     * @param $theories self[]
     * @param $parentTheory self
     * @return string
     */
    public function forwardAndComeBack($theories, $parentTheory)
    {
        $forwardTheory = null;
        $comeBackTheory = null;
        $setForwardId = false;
        $iteratorTheory = null;
        foreach ($theories as $theory) {
            if ($setForwardId) {
                $forwardTheory = $theory;
                $setForwardId = false;
            }
            if ($theory->id == $this->id) {
                $comeBackTheory = $iteratorTheory;
                $setForwardId = true;
            }
            $iteratorTheory = $theory;
            foreach ($theory->children  as $child) {
                if ($setForwardId) {
                    $forwardTheory = $child;
                    $setForwardId = false;
                }
                if ($child->id == $this->id) {
                    $comeBackTheory = $iteratorTheory;
                    $setForwardId = true;
                }
                $iteratorTheory = $child;
            }
        }
        $comBackUrl = $comeBackTheory ? Url::to(['/theory/frontend/view',
            'parentTheoryName' => $parentTheory->getNameForUrl(),
            'theoryName' => $comeBackTheory->getNameForUrl()])
            : null;
        $comeBackHtml = $comBackUrl
            ? '<a class="btn btn-default btn-xs" href="' . $comBackUrl . '">
                    <i class="fa fa-angle-left"></i> Предыдущая
               </a>'
            : '';
        $forwardUrl = $forwardTheory ? Url::to(['/theory/frontend/view',
            'parentTheoryName' => $parentTheory->getNameForUrl(),
            'theoryName' => $forwardTheory->getNameForUrl()])
            : null;
        $forwardHtml = $forwardUrl
            ? ' <a class="btn btn-default btn-xs" href="' . $forwardUrl . '">
                    Следующая <i class="fa fa-angle-right"></i>
               </a>'
            : '';

        return '<div style="text-align: center">' . $comeBackHtml . $forwardHtml . '</div>';
    }
}
