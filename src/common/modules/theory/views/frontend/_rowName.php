<?php

use \common\modules\theory\models\TheorySearch,
    yii\helpers\Html;

/* @var $data TheorySearch */
/* @var $isMainTheory bool */

?>
<div class="style-h2">
    <?= $isMainTheory
        ? Html::a($data->name, ['/theory/frontend/child-theories', 'parentTheory' => $data->id], ['class' => 'text-muted'])
        : '' ?>
</div>

