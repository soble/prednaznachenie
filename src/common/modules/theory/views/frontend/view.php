<?php

/* @var $this yii\web\View */
/* @var $model TheorySearch */
/* @var $parentTheory TheorySearch */

/* @var $childTheories TheorySearch[] */


use \common\modules\theory\models\TheorySearch,
    \yii\helpers\Url,
    \common\models\RelationsViewsToIp;

$this->params['breadcrumbs'][] = [
    'label' => 'Методы',
    'url' => ['/theory/frontend/index']
];
$this->params['breadcrumbs'][] = $this->title;

?>
    <div class="row">
        <div class="col-md-12">
            <div class="mobile-settings-methods hidden-sm hidden-md hidden-lg white-bg">
                <div class="col-xs-12" style="margin-bottom: 10px">
                    <a href="#" class="show-mobile-settings-theory pull-right" style="font-size: 17px">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="style-h2" style="padding: 10px 0; text-align: center">
                            <?= $this->params['leftMenuTitle'] ?>
                        </div>
                        <ul class="nav nav-mobile" id="side-menu">
                            <?= $this->params['leftMenuItems'] ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <button class="show-mobile-settings-theory btn-default btn-sm pull-right hidden-sm hidden-md hidden-lg">
                        <i class="fa fa-sliders"></i>
                    </button>
                    <h1>
                        <?= $model->name ?>
                    </h1>
                </div>
                <div class="methods ibox-content">
                    <div style="margin-bottom: 30px">
                        <?= $model->description ?>
                    </div>
                    <div style="margin-bottom: 30px">
                        <?= $model->forwardAndComeBack($childTheories, $parentTheory) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
$url = Url::to([
    '/theory/frontend/set-count-view',
    'type' => RelationsViewsToIp::TYPE_THEORY,
    'modelId' => $model->id]);
$script = <<< JS
    $.getJSON("https://api.ipify.org/?format=json", function(e) {
        console.log(e.ip);
        $.post('$url' + '&ip=' + e.ip , function( data ) {
            
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_END);
