<?php

/* @var $this yii\web\View */
/* @var $searchModel TheorySearch */
/* @var $isMainTheory bool */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use \common\modules\theory\models\TheorySearch,
    yii\grid\GridView;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="mobile-settings-methods hidden-sm hidden-md hidden-lg white-bg">
            <div class="col-xs-12" style="margin-bottom: 10px">
                <a href="#" class="show-mobile-settings-theory pull-right" style="font-size: 17px">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="nav nav-mobile" id="side-menu">
                        <?= $this->params['leftMenuItems'] ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button class="show-mobile-settings-theory btn-default btn-sm pull-right hidden-sm hidden-md hidden-lg">
                    <i class="fa fa-sliders"></i>
                </button>
                <h1><?= $this->title ?></h1>
            </div>
            <div class="methods ibox-content">
                <p>
                    Все мы разные, и то что применимо к одному человеку, совершенно не подходит к другому.
                    Поэтому существует множество систем деления людей на разные категории, психотипы, социумы,
                    гендерное различие и т.п. Назовем это общим словом "Типизация", что подразумевает под собой
                    определение типа человека по какой-то из выбранных моделей. Этих моделей типизации существует
                    множество - от простейшего визуального деления людей на мужчин и женщин, до тончайшего определения
                    психотипа индивидуума по его астрологической карте.
                </p>
                <p>
                    Методы определения предназначения по различным типам представлены по тэгам "Типизация" в
                    разделе Методы. Некоторые из представленных моделей типизации не очевидны и требуют
                    предварительного ознакомления с теорией. Собственно в данном разделе представлена теория по
                    некоторым видам типизаций.
                </p>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'showHeader' => false,
                    'showOnEmpty' => false,
                    'emptyText' => '',
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
                        [
                            'content' => function ($data) use ($isMainTheory) {
                                return $this->render('_rowName', ['data' => $data, 'isMainTheory' => $isMainTheory]);
                            }
                        ],
                    ],
                    'layout' => "{pager}\n{items}"
                ]); ?>
            </div>
        </div>
    </div>
</div>
