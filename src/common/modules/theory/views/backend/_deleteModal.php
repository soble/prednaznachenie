<?php

/* @var $model MethodsSearch */

use common\modules\methods\models\MethodsSearch;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;


?>
<?php Modal::begin([
    'header' => '<h2>Подтвердите удаление типизации</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-trash"></i> Удалить',
        'class' => 'btn btn btn-danger'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <?php $form = ActiveForm::begin([
                'action' => ['/theory/backend/delete-base', 'id' => $model->id],
                'method' => 'POST'
            ]); ?>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <button class="btn btn btn-danger modal-close" type="submit">
                    Подтвердить
                </button>
            </div>
            <?php $form = ActiveForm::end(); ?>
        </div>
    </div>
<?php Modal::end() ?>
