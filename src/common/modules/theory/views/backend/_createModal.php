<?php

use \common\modules\theory\models\TheoryModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;

$model = new TheoryModel(['scenario' => TheoryModel::SCENARIO_CREATE]);
?>
<?php Modal::begin([
    'header' => '<h2>Создание типизации</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-plus"></i> типизация',
        'class' => 'btn btn btn-primary'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/theory/backend/create', 'currentTheory' => \Yii::$app->request->get('currentTheory')],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
               Создать
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
