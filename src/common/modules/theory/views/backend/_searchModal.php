<?php

use common\modules\methods\models\MethodsSearch;
use \common\modules\theory\models\TheorySearch;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;
use \kartik\select2\Select2;
use \yii\widgets\MaskedInput;

/* @var $searchModel TheorySearch */

?>
<?php Modal::begin([
    'header' => '<h2>Фильтр</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-filter"></i> Фильтр',
        'class' => 'btn btn btn-info'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/theory/backend/index'],
            'method' => 'GET'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($searchModel, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($searchModel, 'status')
                ->widget(Select2::class, [
                    'data' => TheorySearch::statusForSelect(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Статус ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <h4>Период публикации с - по</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <?= $form->field($searchModel, 'datePublishBegin')->widget(MaskedInput::class, [
                        'mask' => '99-99-9999',
                    ])->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <?= $form->field($searchModel, 'datePublishEnd')->widget(MaskedInput::class, [
                        'mask' => '99-99-9999',
                    ])->label(false); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
                Найти
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
