<?php

use common\modules\theory\models\TheorySearch;
use common\widgets\tree\TreeWidget;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel TheorySearch */

?>
<div class="row" style="margin-top: 20px">
    <div class="col-sm-12 col-md-4">
        <h4>
            Дерево
        </h4>
        <?= TreeWidget::widget([
            'items' => TheorySearch::theoryTree(),
            'url' => '/theory/backend/index',
            'urlParams' => ['currentTheory' => 'id'],
        ]) ?>
    </div>
    <div class="col-sm-12 col-md-8">
        <?= $this->render('_createModal') ?>
        <?= $this->render('_searchModal', [
            'searchModel' => $searchModel
        ]) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'Наименование',
                    'content' => function ($data) {
                        return Html::a($data->name, ['/theory/backend/view', 'id' => $data->id]);
                    }
                ],
                [
                    'attribute' => 'created',
                    'label' => 'Создано',
                    'content' => function ($data) {
                        return date('d-m-Y H:i', $data->created);
                    }
                ],
                [
                    'attribute' => 'views',
                    'label' => 'Просмотры',
                    'content' => function ($data) {
                        return $data->views;
                    }
                ],
                [
                    'attribute' => 'date_publish',
                    'label' => 'Опубликованно',
                    'content' => function ($data) {
                        return $data->date_publish ? date('d-m-Y H:i', $data->date_publish) : '';
                    }
                ],
                [
                    'attribute' => 'status',
                    'label' => 'Статус',
                    'content' => function ($data) {
                        return $data->statusHtml();
                    }
                ],
                [
                    'label' => '#',
                    'content' => function ($data) {
                        return Html::a(
                            '<i class="fa fa-trash"></i>',
                            ['/theory/backend/delete', 'id' => $data->id]
                        );
                    }
                ],
            ]
        ]); ?>
    </div>
</div>