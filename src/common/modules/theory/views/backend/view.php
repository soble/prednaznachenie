<?php

/* @var $this yii\web\View */
/* @var $model TheorySearch */

use \common\modules\theory\models\TheorySearch;
use common\widgets\seo\SeoWidget;
use yii\bootstrap\Tabs;

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Типизация',
    'url' => ['/theory/backend/index']
];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $model->name ?></h5>
                <?= $model->statusHtml() ?>
            </div>
            <div class="ibox-content icons-box">
                <?= Tabs::widget([
                    'items' => [
                        [
                            'label' => 'Основное',
                            'content' => $this->render('_form', [
                                'model' => $model
                            ]),
                            'active' => true
                        ],
                        [
                            'label' => 'SEO',
                            'content' => SeoWidget::widget([
                                'isStaticPage' => false,
                                'model' => $model,
                                'url' => '/theory/backend/update'
                            ]),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
