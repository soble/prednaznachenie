<?php

/* @var $this yii\web\View */
/* @var $model TheorySearch */

use common\models\Files;
use common\modules\theory\models\TheorySearch;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use \mihaildev\ckeditor\CKEditor;
use common\widgets\files\FilesWidget;

$script = new JsExpression("
    function handleSubmitButton(e) {
        var name = e.name;
        var hiddenInput = $('#button-name');
        hiddenInput.val(name);
    }
");
$this->registerJs($script, View::POS_HEAD);
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn btn btn-warning',
                'name' => 'save',
                'form' => 'update-form-theory',
                'onclick' => 'handleSubmitButton(this)',
            ]) ?>

            <?= Html::submitButton('Активировать', [
                'class' => 'btn btn-primary',
                'name' => 'activated',
                'form' => 'update-form-theory',
                'onclick' => 'handleSubmitButton(this)',
            ]) ?>
            <a class="btn btn-danger btn-rounded btn-outline"
               href="<?= Url::to(['/theory/backend/delete', 'id' => $model->id]) ?>">
                Отключить
            </a>
            <?= $this->render('_deleteModal', [
                'model' => $model
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin([
            'action' => ['/theory/backend/update', 'id' => $model->id],
            'method' => 'POST',
            'id' => 'update-form-theory'
        ]); ?>
        <?= $form->field($model, 'button')->hiddenInput(['id' => 'button-name'])
            ->label(false) ?>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?= $form->field($model, 'parent_id')
                ->widget(Select2::class, [
                    'data' => $model->tagsForSelectParent(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Родитель ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?= $form->field($model, 'sort')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::class,[
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
    <div class="col-md-12">
        <?= FilesWidget::widget([
            'type' => Files::TYPE_THEORY,
            'modelId' => $model->id,
            'urlForCreate' => Url::to(['/files/upload']),
            'urlForDelete' => '/files/delete'
        ])?>
    </div>
</div>