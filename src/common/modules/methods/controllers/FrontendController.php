<?php

namespace common\modules\methods\controllers;

use common\models\Rating;
use common\modules\methods\models\TitleMethodModel;
use common\modules\seo\models\SeoModel;
use common\modules\methods\models\MethodsSearch;
use common\modules\tags\models\TagsModel;
use common\widgets\seo\SeoWidget;
use common\widgets\tree\TreeWidget;
use \frontend\controllers\MainController;
use yii\helpers\Html;

/**
 * Default controller for the `menu` module
 */
class FrontendController extends MainController
{
    const NAME_MENU = '/methods';

    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', self::NAME_MENU);
        return parent::beforeAction($action);
    }

    public function init()
    {
        $this->leftMenuItems = TreeWidget::widget([
            'items' => TagsModel::tagsListLeftMenuFrontend(TagsModel::getActiveTags()),
            'url' => '/methods/frontend/set-tag',
            'urlParams' => ['tag' => 'id'],
            'forTags' => true
        ]);
        $this->leftMenuTitle = Html::a(
            'Показать всё',
            ['/methods/frontend/delete-all-active-tags'],
            ['class' => 'btn btn-warning btn-xs btn-outline']
        );
        parent::init();
    }

    public function actionIndex()
    {
        SeoWidget::registerStaticPageMetaTags(SeoModel::PAGE_METHODS,
            'Предназначение. Поиск и методы определения');
        $model = new MethodsSearch();
        $model->load(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $model->getMethodsFrontend(),
            'searchModel' => $model
        ]);
    }

    public function actionOrder()
    {
        return $this->actionIndex();
    }

    public function actionView($methodName)
    {
        $model = MethodsSearch::getModelByName($methodName);
        SeoWidget::registerDynamicPageMetaTags($model);
        $model->setView();

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionSetTag($tag)
    {
        TagsModel::setActiveTag($tag);
        TitleMethodModel::setTitle((int)$tag);

        return $this->redirect('/methods');
    }

    public function actionDeleteAllActiveTags()
    {
        TagsModel::deleteAllActiveTags();
        TitleMethodModel::setTitle();

        return $this->redirect('/methods');
    }

    public function actionRating($id)
    {
        $model = MethodsSearch::getModel($id);
        Rating::saveRating(\Yii::$app->request->post(), Rating::TYPE_METHOD, $id);
        \Yii::$app->session->setFlash('success', 'Большое Вам спасибо за оценку!');

        return $this->redirect(['/methods/frontend/view', 'methodName' => $model->getNameForUrl()]);
    }
}
