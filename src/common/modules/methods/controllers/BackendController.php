<?php

namespace common\modules\methods\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\methods\models\MethodModel;
use common\modules\methods\models\MethodsSearch;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_METHODS);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new MethodsSearch();
        $model->load(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $model->getMethodsDataProvider(),
            'searchModel' => $model
        ]);
    }

    public function actionCreate()
    {
        $id = MethodModel::createModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = MethodsSearch::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        MethodModel::updateModel($id, \Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        MethodModel::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Отключено');

        return $this->redirect(['index']);
    }

    public function actionDeleteBase($id)
    {
        MethodModel::deleteModelBase($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
