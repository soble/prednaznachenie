<?php

namespace common\modules\methods\models;

use common\models\MainModel;
use common\models\Rating;
use common\modules\tags\models\TagsModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "methods".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property string $image
 * @property int $views
 * @property int $created
 * @property int $updated
 * @property string $difficulty
 * @property string $icon_color
 * @property string $icon_name
 * @property string $background_for_icon
 * @property string $border_color_for_icon
 * @property string $url
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property int $date_publish
 * @property int $sort
 * @property RelationsMethodToTags[] $tags
 * @property RelationsMethodToTags[] $tagsIndexByName
 * @property Rating[] $ratings
 */
class Methods extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'methods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'seo_title', 'seo_keywords', 'seo_description'], 'string'],
            [['views', 'created', 'updated', 'date_publish', 'sort'], 'integer'],
            [[
                'name',
                'status',
                'image',
                'difficulty',
                'icon_color',
                'icon_name',
                'url',
                'background_for_icon',
                'border_color_for_icon'
            ], 'string', 'max' => 255],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['icon_color', 'default', 'value' => 'FFF']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'status' => 'Статус',
            'image' => 'Картинка',
            'views' => 'Просмотры',
            'created' => 'Создано',
            'updated' => 'Обновлено',
            'difficulty' => 'Сложность',
            'sort' => 'Сортировка',
            'tags' => 'Тэги',
            'background_for_icon' => 'Цвет кружочка',
            'icon_color' => 'Цвет иконки',
            'border_color_for_icon' => 'Цвет границы',
            'seo_title' => 'Title',
            'seo_keywords' => 'Keywords',
            'seo_description' => 'Description'
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value' => time(),
            ],
        ];
    }

    public function getTags()
    {
        return $this->hasMany(RelationsMethodToTags::class, ['method_id' => 'id'])
            ->leftJoin(TagsModel::tableName(), 'tags.id = relations_methods_to_tag.tag_id')
            ->select([
                'name' => 'tags.name',
                'relations_methods_to_tag.*'
            ])->asArray()->indexBy('tag_id');
    }

    public function getTagsIndexByName()
    {
        return $this->hasMany(RelationsMethodToTags::class, ['method_id' => 'id'])
            ->leftJoin(TagsModel::tableName(), 'tags.id = relations_methods_to_tag.tag_id')
            ->select([
                'name' => 'tags.name',
                'relations_methods_to_tag.*'
            ])->asArray()->indexBy('name');
    }

    public function getRatings()
    {
        return $this->hasMany(Rating::class, ['model_id' => 'id'])
            ->where([
                'type' => Rating::TYPE_METHOD,
                'status' => self::STATUS_ACTIVE
            ])->asArray();
    }
}
