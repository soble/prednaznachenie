<?php

namespace common\modules\methods\models;

use common\models\MainModel;
use common\modules\tags\models\TagsModel;

/**
 *
 * @property int $id
 * @property int $method_id
 * @property int $tag_id
 * @property TagsModel $tag
 * @property string $name
 */
class RelationsMethodToTags extends MainModel
{
    public static function tableName()
    {
        return 'relations_methods_to_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'method_id', 'tag_id'], 'integer'],
            [['method_id', 'tag_id'], 'required'],
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'name'
        ]);
    }

    public static function addMethodAndTags($methodId, $tagsIds)
    {
        foreach ($tagsIds as $tagId) {
            $model = new self();
            $model->method_id = $methodId;
            $model->tag_id = $tagId;
            $model->save();
        }
    }
}
