<?php


namespace common\modules\methods\models;

use common\modules\tags\models\TagsModel;
use yii\helpers\Json;

/**
 * Class TitleMethodModel
 * @package common\modules\methods\models
 */
class TitleMethodModel
{
    const TITLE_NAME = 'tags_title_name';
    const TITLE_DEFAULT = 'Методы определения своего Предназначения';

    /**
     * @param int $tagId
     */
    public static function setTitle(int $tagId = 0): void
    {
        $tagModel = TagsModel::findOne($tagId);
        $activeTags = Json::decode(\Yii::$app->session->get(self::TITLE_NAME));
        if (isset($activeTags[$tagId])) {
            unset($activeTags[$tagId]);
            $activeTags = [self::TITLE_DEFAULT];
        } else {
            $activeTags = $tagModel ? [$tagModel->id => $tagModel->name] : [self::TITLE_DEFAULT];
        }
        \Yii::$app->session->set(self::TITLE_NAME, Json::encode($activeTags));
    }

    /**
     * @return string
     */
    public static function getTitle(): string
    {
        $activeTags = Json::decode(\Yii::$app->session->get(self::TITLE_NAME));

        return $activeTags ? current($activeTags) : self::TITLE_DEFAULT;
    }
}
