<?php

namespace common\modules\methods\models;

use common\models\RelationsViewsToIp;
use yii\web\UploadedFile;

/**
 * Class MethodModel
 * @package common\modules\methods\models
 * @property UploadedFile $imageFile
 */
class MethodModel extends Methods
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_RELEASE = 'release';
    const SCENARIO_SAVE_VIEWS = 'views';

    const DIFFICULTY_EASY = 'easy';
    const DIFFICULTY_NORMAL = 'normal';
    const DIFFICULTY_HARD = 'hard';

    const DEFAULT_COLOR = 'default';

    public $imageFile;
    public $button;
    public $tagsArray = [];

    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'created',
                'updated',
                'name',
                'status',
                'description',
                'date_publish',
                'tagsArray',
                'url'
            ], 'required', 'on' => self::SCENARIO_RELEASE],
            ['tagsArray', 'required', 'when' => function($model) {
                return empty($model->tags);
            }, 'on' => self::SCENARIO_RELEASE],
            [['name'], 'required', 'on' => self::SCENARIO_CREATE],
            [['button'], 'string'],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'tagsArray'
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario <> self::SCENARIO_SAVE_VIEWS && !$insert) {
            RelationsMethodToTags::deleteAll(['method_id' => $this->id]);
            if (!empty($this->tagsArray)) {
                RelationsMethodToTags::addMethodAndTags($this->id, $this->tagsArray);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_RELEASE => $this->attributes(),
            self::SCENARIO_SAVE_VIEWS => $this->attributes()
        ]);
    }

    public function upload()
    {
        $uniqName = uniqid($this->id);
        if ($this->validate()) {
            $this->imageFile->saveAs(\Yii::$app->params['pathToSaveImage']
                . 'methods/'
                . $uniqName
                . '.'
                . $this->imageFile->extension);
            $this->image = 'uploads/methods/' . $uniqName . '.' . $this->imageFile->extension;
            return true;
        } else {
            return false;
        }
    }

    public static function createModel($postData)
    {
        $model = new self();
        $model->load($postData);
        $model->save();

        return $model->id;
    }

    public static function updateModel($id, $postData)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = MethodsSearch::getModel($id);
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->imageFile) {
                $model->upload();
            }
            $model->load($postData);
            if ($model->button == 'activated') {
                $model->status = self::STATUS_ACTIVE;
            }

            $model->scenario = $model->status == self::STATUS_ACTIVE
                ? self::SCENARIO_RELEASE
                : self::SCENARIO_DEFAULT;
            $model->date_publish = $model->datePublishForm ? strtotime($model->datePublishForm) : null;
            $model->save();
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $model->id;
    }

    public static function deleteModel($id)
    {
        $model = MethodsSearch::getModel($id);
        $model->status = self::STATUS_DELETED;
        $model->save();
    }

    public static function deleteModelBase($id)
    {
        $model = MethodsSearch::getModel($id);
        $model->delete();
    }

    public function setView()
    {
        $countViews = RelationsViewsToIp::setView(RelationsViewsToIp::TYPE_METHOD, $this->id);
        $this->views = $countViews;
        $this->scenario = self::SCENARIO_SAVE_VIEWS;
        $this->save();
    }

    public function getAvgRating()
    {
        return $this->rating_avg == 0 ? 0 : round($this->rating_avg, 1);
    }
}
