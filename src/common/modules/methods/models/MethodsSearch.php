<?php

namespace common\modules\methods\models;

use common\models\HtmlModel;
use common\models\Rating;
use common\models\RelationsViewsToIp;
use common\modules\tags\models\TagsModel;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\NotFoundHttpException;

/**
 * Class MethodsSearch
 * @package common\modules\methods\models
 * @property int $rating_avg
 * @property int $rating_count
 */
class MethodsSearch extends MethodModel
{
    const PAGE = 1000;

    public $datePublishBegin;
    public $datePublishEnd;

    public $datePublishForm;

    public $difficultyArray;

    public function rules()
    {
        return array_merge(
            parent::rules(),
            [
                [['datePublishBegin', 'datePublishEnd', 'datePublishForm'], 'string'],
                [['methodsFilter', 'tagsArray'], 'safe'],
            ]
        );
    }

    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            [
                'rating_avg',
                'rating_count'
            ]
        );
    }

    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'datePublishBegin' => 'Публикации начало',
                'datePublishEnd' => 'Публикации конец',
                'tagsArray' => 'Тэги',
            ]
        );
    }

    public static function getModel($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Метод не найден');
        }
        $model->tagsArray = array_keys($model->tags);
        $model->datePublishForm = $model->date_publish ? date('d-m-Y', $model->date_publish) : '';

        return $model;
    }

    public static function getModelByName($methodName)
    {
        $model = self::findOne(['url' => $methodName]);
        if (!$model) {
            throw new NotFoundHttpException('Метод не найден');
        }
        $model->tagsArray = array_keys($model->tags);
        $model->datePublishForm = $model->date_publish ? date('d-m-Y', $model->date_publish) : '';

        return $model;
    }

    public static function getMethodsForSearch($searchText)
    {
        return self::find()
            ->where(
                [
                    'methods.status' => self::STATUS_ACTIVE
                ]
            )
            ->andWhere(['or', ['like', 'name', $searchText], ['like', 'description', $searchText]])
            ->with(['tagsIndexByName'])
            ->all();
    }

    public function getMethodsDataProvider()
    {
        $subQuery = self::getSubQueryRatingCount();

        $query = self::find()
            ->select(
                [
                    'methods.*',
                    'rating_count' => "COALESCE(($subQuery), 0)",
                    'rating_avg' => 'COALESCE(round(avg(rating.value), 1), 0)',
                ]
            )
            ->leftJoin(
                Rating::tableName(),
                'rating.model_id = methods.id AND rating.type = \'' . Rating::TYPE_METHOD . '\''
            )
            ->filterWhere(
                [
                    'status' => $this->status,
                    'difficulty' => $this->difficulty
                ]
            )
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['between', 'date_publish', $this->datePublishBegin, $this->datePublishEnd])
            ->andFilterWhere(['>=', 'views', $this->views])
            ->groupBy('methods.id');

        return new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => self::PAGE,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'date_publish' => SORT_DESC,
                    ]
                ],
            ]
        );
    }

    public function getMethodsFrontend()
    {
        $activeTags = TagsModel::getActiveTagsInSession();

        $subQuery = self::getSubQueryRatingCount();

        $query = self::find()
            ->select(
                [
                    'methods.*',
                    'rating_count' => "COALESCE(($subQuery), 0)",
                    'rating_avg' => 'COALESCE(round(avg(rating.value), 1), 0)'
                ]
            )
            ->leftJoin(RelationsMethodToTags::tableName(), 'relations_methods_to_tag.method_id = methods.id')
            ->leftJoin(
                Rating::tableName(),
                'rating.model_id = methods.id AND rating.type=\''
                . Rating::TYPE_METHOD . '\' AND rating.status=\'' . Rating::STATUS_ACTIVE . '\''
            )
            ->where(
                [
                    'methods.status' => self::STATUS_ACTIVE
                ]
            )
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(
                [
                    'relations_methods_to_tag.tag_id' => $activeTags,
                    'methods.difficulty' => $this->difficultyArray
                ]
            )
            ->with(['tags']);
        if (\Yii::$app->request->get(HtmlModel::SORT_BY_DATE)) {
            $query->addOrderBy('date_publish ' . \Yii::$app->request->get(HtmlModel::SORT_BY_DATE));
        }
        if (\Yii::$app->request->get(HtmlModel::SORT_BY_VIEWS)) {
            $query->addOrderBy('views ' . \Yii::$app->request->get(HtmlModel::SORT_BY_VIEWS));
        }
        if (\Yii::$app->request->get(HtmlModel::SORT_BY_RATING)) {
            $sort = \Yii::$app->request->get(HtmlModel::SORT_BY_RATING);
            $query->addOrderBy("rating_avg $sort, rating_count $sort, date_publish $sort");
        }
        if (!\Yii::$app->request->get(HtmlModel::SORT_BY_VIEWS)
            && !\Yii::$app->request->get(HtmlModel::SORT_BY_DATE)
            && !\Yii::$app->request->get(HtmlModel::SORT_BY_RATING)) {
            $query->orderBy('date_publish DESC');
        }
        $query->groupBy('methods.id');

        $data = new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => self::PAGE,
                ],
            ]
        );

        $count = self::find()->where(['methods.status' => self::STATUS_ACTIVE])->count();
        $data->setTotalCount($count);

        return $data;
    }

    public static function getNotViewMethods(): ActiveDataProvider
    {
        $subQuery = self::getSubQueryRatingCount();

        $query = self::find()
            ->select(
                [
                    'methods.*',
                    'rating_count' => "COALESCE(($subQuery), 0)",
                    'rating_avg' => 'COALESCE(round(avg(rating.value), 1), 0)',
                ]
            )
            ->leftJoin(
                Rating::tableName(),
                'rating.model_id = methods.id AND rating.type = \'' . Rating::TYPE_METHOD . '\''
            )
            ->where(['methods.status' => self::STATUS_ACTIVE])
            ->andWhere(
                [
                    'not in',
                    'methods.id',
                    (new Query())->select('model_id')
                        ->from(RelationsViewsToIp::tableName())
                        ->where(
                            [
                                'type_model' => RelationsViewsToIp::TYPE_METHOD,
                                'hash' => RelationsViewsToIp::getHashCurrentUser()
                            ]
                        )
                ]
            )
            ->orderBy('random()')
            ->groupBy('methods.id')
            ->limit(3);

        return new ActiveDataProvider(
            [
                'query' => $query,
                'pagination' => [
                    'pageSize' => 3,
                ],
            ]
        );
    }

    public function getStyleIconForRow()
    {
        $style = 'padding: 9px 10px; font-size: 22px;';
        $backgroundColor = $this->background_for_icon
            ? 'background-color: #' . $this->background_for_icon . ';'
            : 'transparent';
        $iconColor = $this->icon_color
            ? 'color: #' . $this->icon_color . ';'
            : 'white';
        $borderColor = $this->border_color_for_icon
            ? 'border: 1px solid #' . $this->border_color_for_icon . ';'
            : 'transparent';

        return $style . $backgroundColor . $iconColor . $borderColor;
    }

    public static function getSubQueryRatingCount(): string
    {
        return "select count(sub_rating.id) 
            from rating as sub_rating 
            where sub_rating.model_id = methods.id 
                and sub_rating.type = '" . Rating::TYPE_METHOD . "'
                and sub_rating.status='" . Rating::STATUS_ACTIVE ."'";
    }
}
