<?php

/* @var $this yii\web\View */
/* @var $model MethodsSearch */

use common\models\Files;
use common\modules\methods\models\MethodsSearch;
use common\modules\tags\models\TagsModel;
use common\widgets\files\FilesWidget;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

$script = new JsExpression("
    function handleSubmitButton(e) {
        var name = e.name;
        var hiddenInput = $('#button-name');
        hiddenInput.val(name);
    }
");
$this->registerJs($script, View::POS_HEAD);
?>
<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn btn btn-warning',
                'name' => 'save',
                'form' => 'update-form-methods',
                'onclick' => 'handleSubmitButton(this)',
            ]) ?>

            <?= Html::submitButton('Активировать', [
                'class' => 'btn btn-primary',
                'name' => 'activated',
                'form' => 'update-form-methods',
                'onclick' => 'handleSubmitButton(this)',
            ]) ?>
            <a class="btn btn-danger btn-rounded btn-outline"
               href="<?= Url::to(['/methods/backend/delete', 'id' => $model->id]) ?>">
                Отключить
            </a>
            <?= $this->render('_deleteModal', [
                'model' => $model
            ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php $form = ActiveForm::begin([
            'action' => ['/methods/backend/update', 'id' => $model->id],
            'method' => 'POST',
            'id' => 'update-form-methods'
        ]); ?>
        <?= $form->field($model, 'button')->hiddenInput(['id' => 'button-name'])
            ->label(false) ?>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <?= $form->field($model, 'datePublishForm')
                ->widget(MaskedInput::class, [
                    'mask' => '99-99-9999',
                ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'tagsArray')
                ->widget(Select2::class, [
                    'data' => TagsModel::getTagsForSelect(),
                    'options' => [
                        'multiple' => true,
                        'placeholder' => 'Тэги ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-2">
            <?= $form->field($model, 'background_for_icon')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2">
            <?= $form->field($model, 'icon_color')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2">
            <?= $form->field($model, 'border_color_for_icon')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2">
            <?= $form->field($model, 'icon_name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?= $form->field($model, 'url')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::class, [
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
    <div class="col-md-12">
        <?= FilesWidget::widget([
            'type' => Files::TYPE_METHODS,
            'modelId' => $model->id,
            'urlForCreate' => Url::to(['/files/upload']),
            'urlForDelete' => '/files/delete'
        ]) ?>
    </div>
</div>