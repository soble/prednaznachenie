<?php

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel MethodsSearch */

use common\modules\description\models\DescriptionStaticPage;
use common\modules\seo\models\SeoModel;
use \common\modules\methods\models\MethodsSearch;
use common\widgets\seo\SeoWidget;
use yii\bootstrap\Tabs;

$this->title = 'Методы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Методы
                    <small class="m-l-sm">Список методов</small>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <?= Tabs::widget([
                    'items' => [
                        [
                            'label' => 'Основное',
                            'content' => $this->render('_list', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                            ]),
                            'active' => true
                        ],
                        [
                            'label' => 'SEO',
                            'content' => SeoWidget::widget(['pageName' => SeoModel::PAGE_METHODS]),
                        ],
                        [
                            'label' => 'Описание',
                            'content' => DescriptionStaticPage::viewEditor(DescriptionStaticPage::PAGE_METHODS),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
