<?php

use \common\modules\methods\models\MethodModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;

$model = new MethodModel(['scenario' => MethodModel::SCENARIO_CREATE]);
?>
<?php Modal::begin([
    'header' => '<h2>Создание метода</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-plus"></i> Метод',
        'class' => 'btn btn btn-primary'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/methods/backend/create'],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
               Создать
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
