<?php

use common\modules\methods\models\MethodsSearch;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel MethodsSearch */

?>
<div style="margin-top: 20px">
    <?= $this->render('_createModal') ?>
    <?= $this->render('_searchModal', [
        'searchModel' => $searchModel
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'contentOptions' => ['style' => 'vertical-align: middle'],
                'options' => ['width' => '50'],
                'content' => function ($data) {
                    return Html::a('<i class="fa fa-'
                        . str_replace('fa-', '', $data->icon_name) . '"></i>',
                        ['/methods/backend/view', 'id' => $data->id],
                        [
                            'class' => 'btn btn-circle btn-lg',
                            'style' => $data->getStyleIconForRow()
                        ]);
                }
            ],
            [
                'attribute' => 'name',
                'label' => 'Наименование',
                'content' => function ($data) {
                    return Html::a($data->name, ['/methods/backend/view', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'created',
                'label' => 'Создано',
                'content' => function ($data) {
                    return date('d-m-Y H:i', $data->created);
                }
            ],
            [
                'attribute' => 'views',
                'label' => 'Просмотры',
                'content' => function ($data) {
                    return $data->views;
                }
            ],
            [
                'attribute' => 'rating_avg',
                'label' => 'Оценка',
                'content' => function ($data) {
                    return ($data->rating_avg ? round($data->rating_avg, 1) : 0) . ' (' . ($data->rating_count) . ')';
                }
            ],
            [
                'attribute' => 'date_publish',
                'label' => 'Опубликованно',
                'content' => function ($data) {
                    return $data->date_publish ? date('d-m-Y', $data->date_publish) : '';
                }
            ],
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'content' => function ($data) {
                    return $data->statusHtml();
                }
            ],
            [
                'label' => '#',
                'content' => function ($data) {
                    return Html::a(
                        '<i class="fa fa-trash"></i>',
                        ['/methods/backend/delete', 'id' => $data->id]
                    );
                }
            ],
        ]
    ]); ?>
</div>
