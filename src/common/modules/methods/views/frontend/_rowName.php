<?php

use common\modules\methods\models\MethodsSearch;
use yii\helpers\Html;

/* @var $data MethodsSearch */

?>
<div class="pull-right" title="Опубликовано" style="margin-top: 5px; padding-left: 5px">
    <?= ($data->date_publish ? date('d-m-Y', $data->date_publish) : '') ?>
</div>
<div class="style-h2">
    <?= Html::a($data->name, ['/methods/frontend/view',
        'methodName' => $data->getNameForUrl()], ['class' => 'text-muted']) ?>
</div>
<div class="tags">
    <div class="pull-right" style="margin-left: 10px" title="Просмотров">
        <i class="fa fa-eye"></i> <?= $data->views ? $data->getViews() : 0 ?>
    </div>
    <div class="pull-right" title="Рейтинг (Оценок)">
        <i class="fa fa-thumbs-o-up"></i> <?= $data->getAvgRating() ?> (<?= $data->rating_count ?>)
    </div>
    <?php foreach ($data->tags as $tag) : ?>
        <span class="simple_tag"><i class="fa fa-tag"></i> <?= $tag['name'] ?></span>
    <?php endforeach; ?>
</div>
