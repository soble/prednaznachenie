<?php

/* @var $this yii\web\View */
/* @var $searchModel MethodsSearch */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use common\modules\description\models\DescriptionStaticPage;
use \common\modules\methods\models\MethodsSearch,
    yii\grid\GridView,
    yii\helpers\Html,
    \common\models\HtmlModel,
    \yii\helpers\Url,
    common\modules\methods\models\TitleMethodModel;

$this->title = TitleMethodModel::getTitle();
$this->params['breadcrumbs'][] = $this->title;

$showAll = TitleMethodModel::TITLE_DEFAULT == TitleMethodModel::getTitle() ?
    '' : "<span style='margin-left: 6px'>
        (<a class='hover-underline' href='/methods/frontend/delete-all-active-tags'>показать всё</a>)";
?>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="mobile-settings-methods hidden-sm hidden-md hidden-lg white-bg">
            <div class="col-xs-12" style="margin-bottom: 10px">
                <a href="#" class="show-mobile-settings-theory pull-right" style="font-size: 17px">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p style="padding-left: 20px">
                        <?= HtmlModel::sortButton('/methods/frontend/index', 'xs') ?>
                    </p>
                    <p style="padding-left: 20px">
                        <?= $this->params['leftMenuTitle'] ?>
                    </p>
                    <ul class="nav nav-mobile" id="side-menu">
                        <?= $this->params['leftMenuItems'] ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button class="show-mobile-settings-theory btn-default btn-sm pull-right hidden-sm hidden-md hidden-lg">
                    <i class="fa fa-sliders"></i>
                </button>
                <h1><?= TitleMethodModel::getTitle() ?></h1>
            </div>
            <div class="methods ibox-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <?= DescriptionStaticPage::viewDescription(DescriptionStaticPage::PAGE_METHODS) ?>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="pull-right hidden-xs">
                            Сортировать:
                            <?= HtmlModel::sortButton('/methods/frontend/order') ?>
                        </div>
                    </div>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'showHeader' => false,
                    'tableOptions' => ['class' => 'table table-hover'],
                    'summary' => "Показано методов <b>{count}</b> из <b>{totalCount}</b> $showAll",
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        return [
                            'data-url' => Url::to(['/methods/frontend/view',
                                'methodName' => $model->getNameForUrl()]),
                            'class' => 'view-method-row'
                        ];
                    },
                    'columns' => [
                        [
                            'contentOptions' => [
                                'style' => 'vertical-align: middle',
                            ],
                            'options' => ['width' => '50'],
                            'content' => function ($data) {
                                return Html::a('<i class="fa fa-'
                                    . str_replace('fa-', '', $data->icon_name) . '"></i>',
                                    ['/methods/frontend/view',
                                        'methodName' => $data->getNameForUrl()],
                                    [
                                        'class' => 'btn btn-circle btn-lg ',
                                        'style' => $data->getStyleIconForRow()
                                    ]);
                            }
                        ],
                        [
                            'content' => function ($data) {
                                return $this->render('_rowName', ['data' => $data]);
                            }
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
