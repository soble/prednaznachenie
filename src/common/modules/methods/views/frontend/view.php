<?php

/* @var $this yii\web\View */

/* @var $model MethodsSearch */


use \common\modules\methods\models\MethodsSearch,
    \common\models\Rating;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->params['breadcrumbs'][] = [
    'label' => 'Методы',
    'url' => ['/methods/frontend/index']
];
$this->params['breadcrumbs'][] = $this->title;

$ratingModel = new Rating();
$methodsForViewDataProvider = MethodsSearch::getNotViewMethods();
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1>
                    <?= $model->name ?>
                </h1>
            </div>
            <div class="methods ibox-content">
                <?= $model->description ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">

                        <?= Rating::issetRating(Rating::TYPE_METHOD, $model->id)
                            ? ''
                            : $this->render('_ratingForm', [
                                'model' => $model
                            ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if ($methodsForViewDataProvider->count > 0) : ?>
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="methods ibox-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <h2>
                                А вот ещё:
                            </h2>
                            <?= GridView::widget([
                                'dataProvider' => MethodsSearch::getNotViewMethods(),
                                'showHeader' => false,
                                'layout' => '{items}',
                                'tableOptions' => ['class' => 'table table-hover'],
                                'rowOptions' => function ($model, $key, $index, $grid) {
                                    return [
                                        'data-url' => Url::to(['/methods/frontend/view',
                                            'methodName' => $model->getNameForUrl()]),
                                        'class' => 'view-method-row'
                                    ];
                                },
                                'columns' => [
                                    [
                                        'contentOptions' => [
                                            'style' => 'vertical-align: middle',
                                        ],
                                        'options' => ['width' => '50'],
                                        'content' => function ($data) {
                                            return Html::a('<i class="fa fa-'
                                                . str_replace('fa-', '', $data->icon_name) . '"></i>',
                                                ['/methods/frontend/view',
                                                    'methodName' => $data->getNameForUrl()],
                                                [
                                                    'class' => 'btn btn-circle btn-lg ',
                                                    'style' => $data->getStyleIconForRow()
                                                ]);
                                        }
                                    ],
                                    [
                                        'content' => function ($data) {
                                            return $this->render('_rowName', ['data' => $data]);
                                        }
                                    ],
                                ]
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
