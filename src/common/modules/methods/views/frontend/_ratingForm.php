<?php

/* @var $this yii\web\View */
/* @var $model MethodsSearch */

use \common\modules\methods\models\MethodsSearch,
    yii\widgets\ActiveForm,
    kartik\rating\StarRating,
    \himiklab\yii2\recaptcha\ReCaptcha3,
    \common\models\Rating,
    \yii\helpers\Url;

$ratingModel = new Rating();
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/methods/frontend/rating', 'id' => $model->id]),
]) ?>
<div class="style-h2">Ваша оценка</div>
<?= $form->field($ratingModel, 'value')->widget(StarRating::class, [
    'pluginOptions' => [
        'showClear' => false,
        'showCaption' => false,
        'min' => 0,
        'max' => 5,
        'step' => 1,
        'size' => 'xs',
        'filledStar' => '<i class="fa fa-star"></i>',
        'emptyStar' => '<i class="fa fa-star-o"></i>',
    ]
])->label(false); ?>
<?= $form->field($ratingModel, 'captcha')->widget(
    ReCaptcha3::class,
    ['action' => 'rating']
)->label(false) ?>
<button class="btn btn-sm btn-default" type="submit">
    Оценить
</button>
<?php ActiveForm::end(); ?>

