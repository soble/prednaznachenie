<?php

namespace common\modules\tags\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\tags\models\TagsModel;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_TAGS);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate($currentTag = null)
    {
        $id = TagsModel::createModel(\Yii::$app->request->post(), $currentTag);
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = TagsModel::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        TagsModel::updateModel($id,\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionRelease($id)
    {
        TagsModel::release($id);
        \Yii::$app->session->setFlash('success', 'Тэг активирован');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        TagsModel::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
