<?php

namespace common\modules\tags\traits;

use common\modules\tags\models\TagsModel;

trait Html
{

    public $children;
    public $isActive;

    /**
     * @param null|TagsModel $tags
     * @param null $parentId
     * @param null $getName
     * @return self[]
     */
    public static function tagsTree($tags = null, $parentId = null, $getName = null)
    {
        if (!$tags) {
            $tags = TagsModel::find()->orderBy('sort')->all();
        }
        $tagsSort = [];
        $currentTagId = null;
        $getName = $getName ? $getName : 'currentTag';
        if (!empty($tags)) {
            $currentTagId = \Yii::$app->request->get($getName)
                ? \Yii::$app->request->get($getName)
                : current($tags)->id;
        }
        foreach ($tags as $tag) {
            if (!$parentId) {
                if (empty($tag->parent_id)) {
                    $tag->children = self::tagsTree($tags, $tag->id, $getName);
                    $isChildrenActive = array_key_exists('isActive', $tag->children)
                        ? $tag->children['isActive']
                        : false;
                    unset($tag->children['isActive']);
                    $isActive = $currentTagId == $tag->id ? true : $isChildrenActive;
                    $tag->isActive = $isActive;
                    $tagsSort[$tag->id] = $tag;
                }
            } else {
                if ($tag->parent_id == $parentId) {
                    $tag->children = self::tagsTree($tags, $tag->id, $getName);
                    $isChildrenActive = array_key_exists('isActive', $tag->children)
                        ? $tag->children['isActive']
                        : false;
                    unset($tag->children['isActive']);
                    $isActive = $currentTagId == $tag->id ? true : $isChildrenActive;
                    $tag->isActive = $isActive;
                    $tagsSort[$tag->id] = $tag;
                    $tagsSort['isActive'] = isset($tagsSort['isActive']) && $tagsSort['isActive'] == true
                        ? true
                        : $isActive;
                }
            }
        }

        return $tagsSort;
    }

    /**
     * @param null|TagsModel $tags
     * @param null $parentId
     * @return self[]
     */
    public static function tagsListLeftMenuFrontend($tags = null, $parentId = null)
    {
        if (!$tags) {
            $tags = TagsModel::find()->orderBy('sort')->all();
        }
        $tagsSort = [];
        $currentTagId = null;
        $activeTags = TagsModel::getActiveTagsInSession();
        foreach ($tags as $tag) {
            $tag->isActive = isset($activeTags[$tag->id]);
            if (!$parentId) {
                if (empty($tag->parent_id)) {
                    $tag->children = self::tagsListLeftMenuFrontend($tags, $tag->id);
                    $tagsSort[$tag->id] = $tag;
                }
            } else {
                if ($tag->parent_id == $parentId) {
                    $tag->children = self::tagsListLeftMenuFrontend($tags, $tag->id);
                    $tagsSort[$tag->id] = $tag;
                }
            }
        }

        return $tagsSort;
    }
}