<?php

use \common\modules\tags\models\TagsModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;

$model = new TagsModel(['scenario' => TagsModel::SCENARIO_CREATE]);
?>
<?php Modal::begin([
    'header' => '<h2>Создание тэга</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-plus"></i> Тэг',
        'class' => 'btn btn btn-primary'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/tags/backend/create', 'currentTag' => \Yii::$app->request->get('currentTag')],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
               Создать
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
