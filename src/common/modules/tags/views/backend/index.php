<?php

/* @var $this yii\web\View */

use \yii\grid\GridView;
use \common\modules\tags\models\TagsModel;
use \yii\helpers\Html;
use \common\widgets\tree\TreeWidget;
use \yii\helpers\Url;

$this->title = 'Тэги';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Тэги
                    <small class="m-l-sm">Список тэгов</small>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <h4>
                            Дерево
                        </h4>
                        <?= TreeWidget::widget([
                            'items' => TagsModel::tagsTree(),
                            'url' => '/tags/backend/index',
                            'urlParams' => ['currentTag' => 'id'],
                        ]) ?>
                    </div>
                    <div class="col-sm-12 col-md-9">
                        <?= $this->render('_createModal') ?>
                        <?= GridView::widget([
                            'dataProvider' => TagsModel::getTagsDataProvider(),
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'label' => 'Наименование',
                                    'content' => function ($data) {
                                        return Html::a($data->name, ['/tags/backend/view', 'id' => $data->id]);
                                    }
                                ],
                                [
                                    'attribute' => 'sort',
                                    'label' => 'Сортировка',
                                    'content' => function ($data) {
                                        return $data->sort;
                                    }
                                ],
                                [
                                    'attribute' => 'status',
                                    'label' => 'Статус',
                                    'content' => function ($data) {
                                        return $data->statusHtml();
                                    }
                                ],
                                [
                                    'label' => '#',
                                    'content' => function ($data) {
                                        return Html::a(
                                            '<i class="fa fa-trash"></i>',
                                            ['/tags/backend/delete', 'id' => $data->id]
                                        );
                                    }
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

