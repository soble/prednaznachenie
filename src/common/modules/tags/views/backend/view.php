<?php

/* @var $this yii\web\View */
/* @var $model TagsModel */

use common\modules\methods\models\MethodsSearch;
use \common\modules\tags\models\TagsModel;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Тэги',
    'url' => ['/tags/backend/index']
];$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $model->name ?></h5>
                <?= $model->statusHtml() ?>
                <div class="pull-right">
                    <button class="btn btn btn-warning" type="submit" form="update-form-tag">
                        <i class="fa fa-floppy-o"></i> Сохранить
                    </button>
                    <a class="btn btn-primary" href="<?= Url::to(['/tags/backend/release', 'id' => $model->id])?>">
                        Активировать
                    </a>
                    <a class="btn btn-danger" href="<?= Url::to(['/tags/backend/delete', 'id' => $model->id])?>">
                        Удалить
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <div class="row">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin([
                            'action' => ['/tags/backend/update', 'id' => $model->id],
                            'method' => 'POST',
                            'id' => 'update-form-tag'
                        ]); ?>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'sort')->textInput(['autofocus' => true]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <?= $form->field($model, 'parent_id')
                                ->widget(Select2::class, [
                                    'data' => $model->tagsForSelectParent(),
                                    'options' => [
                                        'multiple' => false,
                                        'placeholder' => 'Метод ...',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ]
                                ]) ?>
                        </div>
                        <?php $form = ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
