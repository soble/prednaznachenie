<?php

namespace common\modules\tags\models;

use common\modules\tags\traits\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class TagsModel extends Tags
{
    use Html;

    const PAGE = 20;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_RELEASE = 'release';

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'sort'], 'required', 'on' => self::SCENARIO_RELEASE],
            [['name'], 'required']
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_RELEASE => $this->attributes()
        ]);
    }

    public static function getChildrenTags($parentId, $tags = null)
    {
        if (!$tags) {
            $tags = self::find()->all();
        }
        $childrenTags = [];
        foreach ($tags as $tag) {
            if ($tag->parent_id == $parentId) {
                $tag->children = self::getChildrenTags($tag->id, $tags);
                $childrenTags[$tag->id] = $tag;
            }
        }

        return $childrenTags;
    }

    public static function getTagsDataProvider()
    {
        $currentTag = \Yii::$app->request->get('currentTag');
        $allTags = [];
        if ($currentTag) {
            $childrenTags = self::getChildrenTags($currentTag);
            $allTags = array_merge([$currentTag], array_keys($childrenTags));
        }

        $query = self::find()
            ->andFilterWhere(['id' => $allTags])
            ->orderBy('status ASC');

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
        ]);
    }

    public static function getModel($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new \Exception('Модель не найдена');
        }

        return $model;
    }

    public static function createModel($postData, $currentTag = null)
    {
        $model = new self();
        $model->load($postData);
        if ($currentTag) {
            $model->parent_id = $currentTag;
        }
        $model->save();

        return $model->id;
    }

    public static function updateModel($id, $postData)
    {
        $model = self::getModel($id);
        $model->load($postData);
        $model->save();

        return $model->id;
    }

    public static function release($id)
    {
        $model = self::getModel($id);
        $model->scenario = self::SCENARIO_RELEASE;
        $model->status = self::STATUS_ACTIVE;
        $model->save();
    }

    public static function deleteModel($id)
    {
        $model = self::getModel($id);
        $model->status = self::STATUS_DELETED;
        $model->save();
    }

    public static function getActiveTags()
    {
        return self::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->orderBy('sort')
            ->all();
    }

    public static function getTagsForSelect()
    {
        return ArrayHelper::map(self::getActiveTags(), 'id', 'name');
    }

    public static function getActiveTagsInSession()
    {
        return Json::decode(\Yii::$app->session->get('activeTags'));
    }

    public static function setActiveTag($tagId)
    {
        $tagsTree = self::getTagsTree();
        $activeTags = Json::decode(\Yii::$app->session->get('activeTags'));
        $isSetTag = isset($activeTags[$tagId]) ? true : false;
        if (isset($tagsTree[$tagId])) {
            if (!$isSetTag) {
                $activeTags = [];
            }
            foreach ($tagsTree[$tagId]['childs'] as $child) {
                if ($isSetTag) {
                    unset($activeTags[$child['id']]);
                } else {
                    $activeTags[$child['id']] = $child['id'];
                }
            }
        } else {
            foreach ($tagsTree as $tag) {
                if (isset($tag['childs'][$tagId])) {
                    if (!$isSetTag) {
                        $activeTags = [];
                        $isSetTag = false;
                    }
                    if ($isSetTag && isset($activeTags[$tag['id']])) {
                        $activeTags = [];
                        $isSetTag = false;
                    }
                }
            }
        }
        if ($isSetTag) {
            unset($activeTags[$tagId]);
        } else {
            $activeTags[$tagId] = $tagId;
        }

        \Yii::$app->session->set('activeTags', Json::encode($activeTags));
    }

    public static function deleteAllActiveTags()
    {
        \Yii::$app->session->set('activeTags', null);
    }

    public function tagsForSelectParent()
    {
        return ArrayHelper::map(self::find()->where(['<>', 'id', $this->id])->all(), 'id', 'name');
    }

    public static function getTagsTree($tags = null, $parentId = null)
    {
        if (!$tags) {
            $tags = self::find()->orderBy('sort')->asArray()->all();
        }

        $tagsSort = [];

        foreach ($tags as $tag) {
            if (!$parentId) {
                if (empty($tag['parent_id'])) {
                    $tag['childs'] = self::getTagsTree($tags, $tag['id']);
                    $tagsSort[$tag['id']] = $tag;
                }
            } else {
                if ($tag['parent_id'] == $parentId) {
                    $tag['childs'] = self::getTagsTree($tags, $tag['id']);
                    $tagsSort[$tag['id']] = $tag;
                }
            }
        }

        return $tagsSort;
    }
}
