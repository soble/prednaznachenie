<?php

namespace common\modules\tags\models;

use common\models\MainModel;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $sort
 * @property int $parent_id
 */
class Tags extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'string', 'max' => 255],
            [['sort', 'parent_id'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_NEW]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'status' => 'Статус',
            'sort' => 'Сортировка',
            'parent_id' => 'Родительский тег',
        ];
    }
}
