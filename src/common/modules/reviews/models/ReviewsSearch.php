<?php

namespace common\modules\reviews\models;

use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ReviewsSearch extends ReviewsModel
{
    const PAGE = 20;

    public $dateCreateBeginSearch;
    public $dateCreateEndSearch;
    public $emailSearch;
    public $nameSearch;
    public $statusSearch;
    public $typeSearch;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [[
                'dateCreateBeginSearch',
                'dateCreateEndSearch',
                'emailSearch',
                'nameSearch',
                'statusSearch',
                'typeSearch',
                'createdField'
            ], 'string'],
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_DEFAULT => array_merge($this->attributes(), [
                'dateCreateBeginSearch',
                'dateCreateEndSearch',
                'emailSearch',
                'nameSearch',
                'statusSearch',
                'createdField',
                'typeSearch'])
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'dateCreateBeginSearch' => 'Создание начало',
            'dateCreateEndSearch' => 'Создание конец',
            'emailSearch' => 'Email',
            'nameSearch' => 'Имя',
            'statusSearch' => 'Статус',
            'typeSearch' => 'Тип',
            'createdField' => 'Создано',
        ]);
    }

    public static function getModel($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException('Сообщение не найдено');
        }
        $model->createdField = date('d-m-Y', $model->created);
        return $model;
    }

    public function getCorrectTextAge()
    {
        $textAge = '';
        if ($this->age) {
            $textAge = \Yii::$app->i18n->format(
                '{n, plural, =0{лет} =1{# год} one{# год} few{# года} many{# лет} other{# года}}',
                ['n' => $this->age],
                'ru_RU');
        }

        return $textAge ? ', ' . $textAge : '';
    }

    public function getReviewsDataProvider()
    {
        $query = self::find()
            ->filterWhere([
                'status' => $this->statusSearch,
                'type' => $this->typeSearch
            ])
            ->andFilterWhere(['ilike', 'name', $this->nameSearch])
            ->andFilterWhere(['between', 'date_publish', $this->dateCreateBeginSearch, $this->dateCreateEndSearch])
            ->andFilterWhere(['ilike', 'email', $this->emailSearch]);
        if (!\Yii::$app->request->get('sort')) {
            $query->orderBy(('created DESC'));
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
        ]);
    }

    public function getReviewsFrontend()
    {
        $query = self::find()
            ->where([
                'reviews.status' => self::STATUS_ACTIVE,
                'reviews.type' => self::TYPE_REVIEW,
            ])->orderBy('created DESC');

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
        ]);
    }
}
