<?php

namespace common\modules\reviews\models;

use common\models\MainModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $type
 * @property string $description
 * @property int $created
 * @property int $age
 * @property string $status
 * @property string $error_message
 * @property string $link_to_error
 */
class Reviews extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'error_message', 'link_to_error'], 'string'],
            [['created', 'age'], 'integer'],
            [['created'], 'default'],
            [['name', 'email', 'type', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'Email',
            'type' => 'Тип',
            'description' => 'Описание',
            'created' => 'Дата создания',
            'status' => 'Статус',
            'error_message' => 'Текст ошибки',
            'link_to_error' => 'Ссылка на страницу с ошибкой',
            'age' => 'Возраст',
            'footerType' => 'Тип сообщения',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created']
                ],
                'value' => time(),
            ],
        ];
    }
}
