<?php

namespace common\modules\reviews\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use yii\web\UploadedFile;

/**
 * Class MethodModel
 * @package common\modules\reviews\models
 * @property UploadedFile $imageFile
 */
class ReviewsModel extends Reviews
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_RELEASE = 'release';
    const SCENARIO_ERROR_MESSAGE = 'error_message';
    const SCENARIO_MAIN_CAPTCHA = 'main-captcha';
    const SCENARIO_FOOTER_CAPTCHA = 'footer-captcha';

    const TYPE_ERROR = 'error';
    const TYPE_WISH = 'wish';
    const TYPE_REVIEW = 'review';
    const TYPE_OTHER = 'other';

    public $button;
    public $footerType;
    public $captchaReview;
    public $captchaError;
    public $captchaFooter;
    public $createdField;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['createdField'], 'string'],
            [[
                'name',
                'status',
                'description',
                'email',
                'type',
            ], 'required', 'on' => self::SCENARIO_RELEASE],

            [['name', 'description', 'email', 'type'], 'required', 'on' => self::SCENARIO_CREATE],
            [['name', 'description', 'email', 'type'], 'required', 'on' => self::SCENARIO_MAIN_CAPTCHA],
            [['name', 'description', 'email', 'type', 'footerType'], 'required', 'on' => self::SCENARIO_FOOTER_CAPTCHA],
            [[
                'type',
                'error_message',
                'link_to_error'
            ], 'required', 'on' => self::SCENARIO_ERROR_MESSAGE],
            [['button', 'footerType'], 'string'],
            [['email'], 'email'],
            [['status'], 'default', 'value' => self::STATUS_NEW],
            [['captchaReview'], ReCaptchaValidator3::class,
                'threshold' => 0.1,
                'action' => 'sendReview',
                'message' => \Yii::$app->params['errorMessageCaptcha']],
            [['captchaError'], ReCaptchaValidator3::class,
                'threshold' => 0.1,
                'action' => 'sendError',
                'message' => \Yii::$app->params['errorMessageCaptcha']],
            [['captchaFooter'], ReCaptchaValidator3::class,
                'threshold' => 0.1,
                'action' => 'actionFooter',
                'message' => \Yii::$app->params['errorMessageCaptcha']]
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_RELEASE => array_merge($this->attributes(), ['button', 'footerType', 'createdField']),
            self::SCENARIO_DEFAULT => array_merge($this->attributes(), ['button', 'footerType', 'createdField']),
            self::SCENARIO_MAIN_CAPTCHA => array_merge($this->attributes(), ['captchaReview']),
            self::SCENARIO_FOOTER_CAPTCHA => array_merge($this->attributes(), ['captchaFooter', 'footerType']),
            self::SCENARIO_ERROR_MESSAGE => array_merge($this->attributes(), ['captchaError']),
        ]);
    }

    public static function createModel($postData)
    {
        $model = new self();
        $model->scenario = self::SCENARIO_CREATE;
        $model->load($postData);
        if ($model->footerType) {
            $model->type = $model->footerType;
        }
        $model->save();

        return $model->id;
    }

    public static function createMainListModel($postData)
    {
        $model = new self();
        $model->scenario = self::SCENARIO_MAIN_CAPTCHA;
        $model->load($postData);
        $model->save();

        return $model->id;
    }

    public static function createFooterCaptchaModel($postData)
    {
        $model = new self();
        $model->scenario = self::SCENARIO_FOOTER_CAPTCHA;
        $model->load($postData);
        if ($model->footerType) {
            $model->type = $model->footerType;
        }
        $model->save();

        return $model->id;
    }

    public static function createErrorMessage($postData)
    {
        $model = new self();
        $model->scenario = self::SCENARIO_ERROR_MESSAGE;
        $model->load($postData);
        $model->type = self::TYPE_ERROR;
        $model->link_to_error = \Yii::$app->request->referrer;
        $model->save();

        return $model->id;
    }

    public static function updateModel($id, $postData)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $model = ReviewsSearch::getModel($id);
            $model->scenario = self::SCENARIO_RELEASE;
            $model->load($postData);
            if ($model->button == 'activated') {
                $model->status = self::STATUS_ACTIVE;
            }

            $model->scenario = $model->status == self::STATUS_ACTIVE
                ? self::SCENARIO_RELEASE
                : self::SCENARIO_DEFAULT;
            $model->created = isset($model->createdField) ? strtotime($model->createdField) : $model->created;
            $model->save();
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        return $model->id;
    }

    public static function setStatusNew($id)
    {
        $model = ReviewsSearch::getModel($id);
        $model->status = self::STATUS_NEW;
        $model->save();
    }

    public static function deleteModel($id)
    {
        $model = ReviewsSearch::getModel($id);
        $model->status = self::STATUS_DELETED;
        $model->save();
    }

    public static function deleteModelBase($id)
    {
        $model = ReviewsSearch::getModel($id);
        $model->delete();
    }

    public static function typesMessage()
    {
        return [
            self::TYPE_REVIEW => 'Отзыв',
            self::TYPE_WISH => 'Предложение',
            self::TYPE_OTHER => 'Другое'
        ];
    }

    public static function typesMessageForBackend()
    {
        return [
            self::TYPE_REVIEW => 'Отзыв',
            self::TYPE_WISH => 'Предложение',
            self::TYPE_OTHER => 'Другое',
            self::TYPE_ERROR => 'Ошибка',
        ];
    }

    public function getTypeName()
    {
        return isset(self::typesMessageForBackend()[$this->type]) ? self::typesMessageForBackend()[$this->type] : '';
    }
}
