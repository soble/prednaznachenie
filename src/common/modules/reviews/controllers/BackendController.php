<?php

namespace common\modules\reviews\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\methods\models\MethodModel;
use common\modules\reviews\models\ReviewsModel;
use common\modules\reviews\models\ReviewsSearch;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_REVIEWS);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new ReviewsSearch();
        $model->load(\Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' => $model->getReviewsDataProvider(),
            'searchModel' => $model
        ]);
    }

    public function actionCreate()
    {
        $id = ReviewsModel::createModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = ReviewsSearch::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        ReviewsSearch::updateModel($id, \Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionSetNew($id)
    {
        ReviewsModel::setStatusNew($id);
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        ReviewsSearch::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Отключено');

        return $this->redirect(['index']);
    }

    public function actionDeleteBase($id)
    {
        ReviewsSearch::deleteModelBase($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
