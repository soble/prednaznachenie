<?php

namespace common\modules\reviews\controllers;

use common\modules\seo\models\SeoModel;
use common\modules\reviews\models\ReviewsModel;
use common\modules\reviews\models\ReviewsSearch;
use common\widgets\seo\SeoWidget;
use \frontend\controllers\MainController;

/**
 * Default controller for the `menu` module
 */
class FrontendController extends MainController
{
    const NAME_MENU = '/reviews/frontend';


    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', self::NAME_MENU);
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $model = new ReviewsSearch();
        $model->load(\Yii::$app->request->get());
        SeoWidget::registerStaticPageMetaTags(SeoModel::PAGE_REVIEWS, 'Отзывы');

        return $this->render('index', [
            'dataProvider' => $model->getReviewsFrontend(),
            'searchModel' => $model,
        ]);
    }

    public function actionCreate()
    {
        ReviewsModel::createMainListModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Ваше сообщение отправлено.');

        return $this->redirect('index');
    }

    public function actionCreateFooter()
    {
        ReviewsModel::createFooterCaptchaModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Ваше сообщение отправлено.');

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionErrorMessage()
    {
        ReviewsModel::createErrorMessage(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Большое Вам спасибо за указание на ошибку, мы её исправим 
        в самое ближайшее время!');

        return $this->redirect(\Yii::$app->request->referrer);
    }
}
