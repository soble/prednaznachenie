<?php

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel ReviewsSearch */

use common\modules\description\models\DescriptionStaticPage;
use common\modules\seo\models\SeoModel;
use \common\modules\reviews\models\ReviewsSearch;
use common\widgets\seo\SeoWidget;
use yii\bootstrap\Tabs;

$this->title = 'Сообщения';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Сообщения
                    <small class="m-l-sm">Сообщения</small>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box table-responsive">
                <?= Tabs::widget([
                    'items' => [
                        [
                            'label' => 'Основное',
                            'content' => $this->render('_list', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                            ]),
                            'active' => true
                        ],
                        [
                            'label' => 'SEO',
                            'content' => SeoWidget::widget(['pageName' => SeoModel::PAGE_REVIEWS]),
                        ],
                        [
                            'label' => 'Описание',
                            'content' => DescriptionStaticPage::viewEditor(DescriptionStaticPage::PAGE_REVIEWS),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
