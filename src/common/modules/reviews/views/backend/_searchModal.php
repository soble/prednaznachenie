<?php

use common\modules\reviews\models\ReviewsSearch;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;
use \kartik\select2\Select2;
use \yii\widgets\MaskedInput;

/* @var $searchModel ReviewsSearch */

?>
<?php Modal::begin([
    'header' => '<h2>Фильтр</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-filter"></i> Фильтр',
        'class' => 'btn btn btn-info'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/reviews/backend/index'],
            'method' => 'GET'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($searchModel, 'nameSearch')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($searchModel, 'statusSearch')
                ->widget(Select2::class, [
                    'data' => ReviewsSearch::statusForSelect(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Статус ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($searchModel, 'typeSearch')
                ->widget(Select2::class, [
                    'data' => ReviewsSearch::typesMessageForBackend(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Тип ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <h4>Период создания с - по</h4>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <?= $form->field($searchModel, 'dateCreateBeginSearch')->widget(MaskedInput::class, [
                        'mask' => '99-99-9999',
                    ])->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <?= $form->field($searchModel, 'dateCreateEndSearch')->widget(MaskedInput::class, [
                        'mask' => '99-99-9999',
                    ])->label(false); ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
                Найти
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
