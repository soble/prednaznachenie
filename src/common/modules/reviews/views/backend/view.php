<?php

/* @var $this yii\web\View */
/* @var $model ReviewsSearch */

use \common\modules\reviews\models\ReviewsSearch;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html,
    \yii\web\JsExpression,
    \yii\web\View;

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Сообщения',
    'url' => ['/reviews/backend/index']
];
$this->params['breadcrumbs'][] = $this->title;

$script = new JsExpression("
    function handleSubmitButton(e) {
        var name = e.name;
        var hiddenInput = $('#button-name');
        hiddenInput.val(name);
    }
");
$this->registerJs($script, View::POS_HEAD);
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $model->email ?></h5>
                <?= $model->statusHtml() ?>
                <div class="pull-right">
                    <?= Html::a('-> Новый',
                        ['/reviews/backend/set-new', 'id' => $model->id],
                        ['class' => 'btn btn btn-default'])
                    ?>

                    <?= Html::submitButton('Сохранить', [
                        'class' => 'btn btn btn-warning',
                        'name' => 'save',
                        'form' => 'update-form-reviews',
                        'onclick' => 'handleSubmitButton(this)',
                    ]) ?>

                    <?= Html::submitButton('Активировать', [
                        'class' => 'btn btn-primary',
                        'name' => 'activated',
                        'form' => 'update-form-reviews',
                        'onclick' => 'handleSubmitButton(this)',
                    ]) ?>
                    <a class="btn btn-danger btn-rounded btn-outline"
                       href="<?= Url::to(['/reviews/backend/delete', 'id' => $model->id]) ?>">
                        Отключить
                    </a>
                    <?= $this->render('_deleteModal', [
                            'model' => $model
                    ]) ?>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <div class="row">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin([
                            'action' => ['/reviews/backend/update', 'id' => $model->id],
                            'method' => 'POST',
                            'id' => 'update-form-reviews'
                        ]); ?>
                        <?= $form->field($model, 'button')->hiddenInput(['id' => 'button-name'])
                            ->label(false) ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <?= $form->field($model, 'name')->textInput() ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <?= $form->field($model, 'email')->textInput() ?>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <?= $form->field($model, 'type')
                                ->widget(Select2::class, [
                                    'data' => $model::typesMessageForBackend(),
                                    'options' => [
                                        'multiple' => false,
                                        'placeholder' => 'Тип сообщения...',
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ]
                                ]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <label class="control-label" for="reviewssearch-name">Ссылка на ошибку: </label>
                            <?= $model->link_to_error ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <?= $form->field($model, 'age')->textInput() ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <?= $form->field($model, 'createdField')->textInput() ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'error_message')->textarea(['rows' => 5]) ?>
                        </div>
                        <div class="col-md-12">
                            <?= $form->field($model, 'description')->textarea(['rows' => 5]) ?>
                        </div>
                        <?php $form = ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
