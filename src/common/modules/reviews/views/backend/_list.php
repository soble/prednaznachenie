<?php

use common\modules\reviews\models\ReviewsSearch;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel ReviewsSearch */
?>
<div style="margin-top: 20px">
    <?= $this->render('_searchModal', [
        'searchModel' => $searchModel
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'status',
                'label' => 'Статус',
                'content' => function ($data) {
                    return $data->statusHtml();
                }
            ],
            [
                'attribute' => 'type',
                'label' => 'Тип',
                'content' => function ($data) {
                    return Html::a($data->getTypeName(), ['/reviews/backend/view', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'name',
                'label' => 'Имя',
                'content' => function ($data) {
                    return Html::a($data->name, ['/reviews/backend/view', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'age',
                'label' => 'Воз',
                'content' => function ($data) {
                    return $data->age;
                }
            ],
            [
                'attribute' => 'email',
                'label' => 'Email',
                'content' => function ($data) {
                    return Html::a($data->email, ['/reviews/backend/view', 'id' => $data->id]);
                }
            ],
            [
                'attribute' => 'description',
                'content' => function ($data) {
                    return $data->description;
                }
            ],
            [
                'attribute' => 'link_to_error',
                'label' => 'Ссылка',
                'content' => function ($data) {
                    return $data->link_to_error ? Html::a('ссылка', $data->link_to_error, [
                        'target' => '_blank'
                    ]) : '';
                }
            ],
            [
                'attribute' => 'error_message',
                'content' => function ($data) {
                    return $data->error_message;
                }
            ],
            [
                'attribute' => 'created',
                'label' => 'Создано',
                'content' => function ($data) {
                    return date('d-m-Y H:i', $data->created);
                }
            ],
            [
                'label' => '#',
                'content' => function ($data) {
                    return $this->render('_deleteModal', [
                        'model' => $data,
                        'smallButton' => true
                    ]);
                }
            ],
        ]
    ]); ?>
</div>