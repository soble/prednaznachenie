<?php

/* @var $model ReviewsSearch */
/* @var $smallButton boolean */

use common\modules\reviews\models\ReviewsSearch;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm;


?>
<?php Modal::begin([
    'header' => '<h2>Подтвердите удаление сообщения</h2>',
    'toggleButton' => [
        'label' => (isset($smallButton) ? '<i class="fa fa-trash"></i>' : '<i class="fa fa-trash"></i> Удалить'),
        'class' => (isset($smallButton) ? 'btn btn btn-default btn-xs' : 'btn btn btn-danger')
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <?php $form = ActiveForm::begin([
                'action' => ['/reviews/backend/delete-base', 'id' => $model->id],
                'method' => 'POST'
            ]); ?>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <button class="btn btn btn-danger modal-close" type="submit">
                    Подтвердить
                </button>
            </div>
            <?php $form = ActiveForm::end(); ?>
        </div>
    </div>
<?php Modal::end() ?>
