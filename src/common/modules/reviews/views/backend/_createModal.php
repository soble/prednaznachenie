<?php

use \common\modules\reviews\models\ReviewsModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm,
    kartik\select2\Select2;

$model = new ReviewsModel(['scenario' => ReviewsModel::SCENARIO_CREATE]);
?>
<?php Modal::begin([
    'header' => '<h2>Создание сообщения</h2>',
    'toggleButton' => [
        'label' => '<i class="fa fa-plus"></i> Сообщение',
        'class' => 'btn btn btn-primary'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/reviews/backend/create'],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'type')
                ->widget(Select2::class, [
                    'data' => $model::typesMessageForBackend(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Тип сообщения...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->textarea(['autofocus' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder modal-close" type="submit">
               Создать
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
