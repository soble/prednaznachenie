<?php

use \common\modules\reviews\models\ReviewsModel;
use yii\bootstrap\Modal,
    \yii\widgets\ActiveForm,
    himiklab\yii2\recaptcha\ReCaptcha3;

$model = new ReviewsModel(['scenario' => ReviewsModel::SCENARIO_ERROR_MESSAGE]);
?>
<a href="#" class="pull-right add-select-text" onclick="getSelect()" style="font-size: 10px; line-height: 1">
    <span class="hidden-xs">Сообщить <br /> об ошибке</span>
    <span class="hidden-sm hidden-md hidden-lg" style="font-size: 16px">
        <i class="fa fa-exclamation-triangle" style="margin-top: 2px"></i>
    </span>
</a>
<?php Modal::begin([
    'header' => '<div class="style-h2">Сообщение об ошибке</div>',
    'id' => 'error-message-modal',
    'toggleButton' => false,
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/reviews/frontend/error-message'],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'error_message')->textarea([
                'class' => 'form-control error-message',
                'style' => 'color: red',
                'readonly' => true,
                'rows' => 7
            ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->textarea([
                'rows' => 7
            ]) ?>
        </div>
        <?= $form->field($model, 'captchaError')->widget(
            ReCaptcha3::class,
            ['action' => 'sendError']
        )->label(false) ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn-primary modal-close" type="submit">
                Отправить
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end();
Modal::begin([
    'header' => false,
    'headerOptions' => ['style' => 'border-bottom: none;'],
    'id' => 'alert-message-modal',
    'toggleButton' => false,
    'size' => 'modal-sm',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-md-12" style="text-align: center">
        <div class="style-h2" style="text-align: center">Пожалуйста выделите<br/> текст с ошибкой</div>
        <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">
            OK
        </button>
    </div>
</div>
<?php
Modal::end();
$script = <<< JS
    function getSelect(e) {
        if (window.getSelection().toString() == '') {
            $('#alert-message-modal').modal('toggle');
        } else {
            $('#error-message-modal').modal('toggle');
            $('.error-message').val(window.getSelection().toString());
        }
    }
JS;
$this->registerJs($script, yii\web\View::POS_END);
