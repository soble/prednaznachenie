<?php

/* @var $this yii\web\View */
/* @var $model ReviewsSearch */

use \common\modules\reviews\models\ReviewsSearch,
    \yii\helpers\Html;

?>
<p>
    <span class="pull-right">
        <?= $model->created ? date('d-m-Y', $model->created) : ''?>
    </span>
    <?= Html::encode($model->name) ?><?= $model->getCorrectTextAge() ?>
</p>
<p>
    "<?= Html::encode($model->description) ?>"
</p>
