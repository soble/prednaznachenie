<?php

use \common\modules\reviews\models\ReviewsModel;
use yii\bootstrap\Modal;
use \yii\widgets\ActiveForm,
    kartik\select2\Select2,
    himiklab\yii2\recaptcha\ReCaptcha3;

$model = new ReviewsModel(['scenario' => ReviewsModel::SCENARIO_MAIN_CAPTCHA]);
$model->type = $model::TYPE_REVIEW;
?>
<?php Modal::begin([
    'header' => '<div class="style-h2">Напишите нам</div>',
    'toggleButton' => [
        'label' => 'Написать отзыв',
        'class' => 'btn btn btn-primary pull-right'
    ],
    'size' => 'modal-md',
    'options' => [
        'tabindex' => false
    ]
]); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/reviews/frontend/create'],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'type')
                ->widget(Select2::class, [
                    'data' => $model::typesMessage(),
                    'options' => [
                        'multiple' => false,
                        'placeholder' => 'Тип сообщения...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ]
                ]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-8">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <?= $form->field($model, 'age')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'email')->textInput() ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->textarea([
                'rows' => 7
            ]) ?>
        </div>
        <?= $form->field($model, 'captchaReview')->widget(
            ReCaptcha3::class,
            ['action' => 'sendReview']
        )->label(false) ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-primary create-folder modal-close" type="submit">
               Отправить
            </button>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>
<?php Modal::end() ?>
