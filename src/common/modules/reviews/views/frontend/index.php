<?php

/* @var $this yii\web\View */
/* @var $searchModel ReviewsSearch */
/* @var $leftBarContent string */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use common\modules\description\models\DescriptionStaticPage;
use \common\modules\reviews\models\ReviewsSearch,
    yii\grid\GridView;

$this->params['breadcrumbs'][] = $this->title;
$view = $this;
?>

<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <?= $this->render('_createModal') ?>
                <h1>Отзывы</h1>
            </div>
            <div class="reviews ibox-content">
                <?= DescriptionStaticPage::viewDescription(DescriptionStaticPage::PAGE_REVIEWS) ?>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="reviews ibox-content">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'showHeader' => false,
                    'showOnEmpty' => false,
                    'emptyText' => '',
                    'tableOptions' => ['class' => 'table table-hover'],
                    'columns' => [
                        [
                            'content' => function ($data) use ($view) {
                                return $view->render('_row', [
                                    'model' => $data
                                ]);
                            }
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
