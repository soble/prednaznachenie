<?php

namespace common\modules\seo\controllers;

use backend\controllers\MainController;
use common\modules\seo\models\SeoModel;
use common\modules\seo\models\SiteMapModel;

/**
 * Default controller for the `seo` module
 */
class SeoController extends MainController
{
    public function actionCreateOrUpdate($pageName)
    {
        SeoModel::createOrUpdate($pageName, \Yii::$app->request->post());
        \Yii::$app->session->setFlash('success' , 'Данные сохранены');

        return $this->redirect(\Yii::$app->request->referrer);
    }
}
