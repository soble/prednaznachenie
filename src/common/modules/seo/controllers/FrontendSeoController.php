<?php

namespace common\modules\seo\controllers;

use \frontend\controllers\MainController;
use common\modules\seo\models\SeoModel;
use common\modules\seo\models\SiteMapModel;

/**
 * Default controller for the `seo` module
 */
class FrontendSeoController extends MainController
{
    public function actionSiteMap()
    {
        return SiteMapModel::getXml();
    }
}
