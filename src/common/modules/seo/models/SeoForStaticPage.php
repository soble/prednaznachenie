<?php

namespace common\modules\seo\models;

use common\models\MainModel;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "seo_for_static_page".
 *
 * @property int $id
 * @property string $page_name
 * @property integer $created
 * @property string $title
 * @property string $keywords
 * @property string $description
 */
class SeoForStaticPage extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seo_for_static_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created'], 'integer'],
            [['keywords', 'description'], 'string'],
            [['page_name', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_name_page' => 'Table Name Page',
            'created' => 'Created',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created'],
                ],
                'value' => time(),
            ],
        ];
    }
}
