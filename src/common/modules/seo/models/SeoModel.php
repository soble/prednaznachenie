<?php

namespace common\modules\seo\models;

class SeoModel extends SeoForStaticPage
{
    const PAGE_ABOUT = 'about';
    const PAGE_TYPES = 'types';
    const PAGE_METHODS = 'methods';
    const PAGE_REVIEWS= 'reviews';

    public function rules()
    {
        return array_merge(parent::rules(), [
            ['page_name', 'required']
        ]);
    }

    public static function createOrUpdate($pageName, $postData)
    {
        if (!$model = self::findOne(['page_name' => $pageName])) {
            $model = new self();
        }
        $model->load($postData);
        $model->save();
    }

    public static function getModelByPageName($pageName)
    {
        return self::findOne(['page_name' => $pageName]);
    }
}
