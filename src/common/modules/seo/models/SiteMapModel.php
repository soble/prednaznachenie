<?php

namespace common\modules\seo\models;

use common\modules\methods\models\MethodModel;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Response;

/**
 * Class SiteMapModel
 * @package common\models
 * @property  string xml
 */
class SiteMapModel extends Model
{
    public static function getXml()
    {
//        \Yii::$app->cache->delete('siteMap');
        if (!$xmlSiteMap = \Yii::$app->cache->get('siteMap')) {
            $urls = [
                [
                    'loc' => Url::to(['/reviews/frontend'], 'https'),
                    'lastmod' => date(DATE_W3C, time()),
                    'changefreq' => 'daily',
                    'priority' => 1.0
                ],
                [
                    'loc' => Url::to(['/methods/frontend/index'], 'https'),
                    'lastmod' => date(DATE_W3C, time()),
                    'changefreq' => 'daily',
                    'priority' => 1.0
                ],
            ];
            $methods = MethodModel::findAll(['status' => MethodModel::STATUS_ACTIVE]);
            foreach ($methods as $method) {
                $urls[] = [
                    'loc' => Url::to(['/methods/frontend/view', 'methodName' => $method->getNameForUrl()], 'https'),
                    'lastmod' => date(DATE_W3C, $method->updated),
                    'changefreq' => 'daily',
                    'priority' => 1.0
                ];
            }
            $xmlSiteMap = \Yii::$app->view->render('@common/modules/seo/views/seo/siteMap', [
                'urls' => $urls,
            ]);

            \Yii::$app->cache->set('siteMap', $xmlSiteMap, 60 * 60 * 12);
        }

        \Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        return $xmlSiteMap;
    }
}
