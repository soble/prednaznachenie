<?php

use common\modules\description\models\DescriptionStaticPage;
use mihaildev\ckeditor\CKEditor;
use yii\widgets\ActiveForm;

/** @var $model DescriptionStaticPage */
?>
<div class="row" style="margin-top: 20px">
    <div class="col-xs-12 col-sm-12 col-md-12 ">
        <?php $form = ActiveForm::begin([
            'action' => ['/description/backend/save', 'pageName' => $model->page_name],
            'method' => 'POST'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <button class="btn btn btn-success create-folder pull-right" type="submit">
                Сохранить
            </button>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'text')->widget(CKEditor::class, [
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
</div>

