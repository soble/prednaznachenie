<?php

namespace common\modules\description\models;

use common\models\MainModel;

/**
 * This is the model class for table "description_static_page".
 *
 * @property int $id
 * @property string $page_name
 * @property string $text
 */
class DescriptionStaticPage extends MainModel
{
    const PAGE_METHODS = 'methods';
    const PAGE_TYPES = 'types';
    const PAGE_REVIEWS = 'reviews';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'description_static_page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['page_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_name' => 'Page Name',
            'text' => 'Описание',
        ];
    }

    public static function saveModel($pageName, $postData)
    {
        $model = self::findOne(['page_name' => $pageName]);
        if (!$model) {
            throw new \Exception('Модель не найдена');
        }
        $model->load($postData);
        $model->save();
    }

    public static function viewEditor($pageName)
    {
        $model = self::findOne(['page_name' => $pageName]);
        if (!$model) {
            throw new \Exception('Модель не найдена');
        }

        return \Yii::$app->controller->renderPartial('@common/modules/description/views/backend/editor', [
            'model' => $model
        ]);
    }

    public static function viewDescription($pageName)
    {
        $model = self::findOne(['page_name' => $pageName]);

        return $model ? $model->text : '';
    }
}
