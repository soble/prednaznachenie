<?php

namespace common\modules\description\controllers;

use common\modules\description\models\DescriptionStaticPage;
use yii\web\Controller;

/**
 * Default controller for the `description` module
 */
class BackendController extends Controller
{
    public function actionSave($pageName)
    {
        DescriptionStaticPage::saveModel($pageName, \Yii::$app->request->post());

        return $this->redirect(\Yii::$app->request->referrer);
    }
}
