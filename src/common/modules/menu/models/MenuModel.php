<?php

namespace common\modules\menu\models;

use common\modules\menu\traits\HtmlFront;
use yii\data\ActiveDataProvider;

class MenuModel extends MainMenu
{
    use HtmlFront;

    const PAGE = 20;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_RELEASE = 'release';

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'sort', 'status', 'link'], 'required', 'on' => self::SCENARIO_RELEASE],
            [['name'], 'required']
        ]);
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => $this->attributes(),
            self::SCENARIO_RELEASE => $this->attributes()
        ]);
    }

    public static function getMenuDataProvider()
    {
        $query = self::find();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => self::PAGE,
            ],
            'sort' => [
                'defaultOrder' => [
                    'sort' => SORT_DESC,
                ]
            ],
        ]);
    }

    public static function getModel($id)
    {
        $model = self::findOne($id);
        if (!$model) {
            throw new \Exception('Модель не найдена');
        }

        return $model;
    }

    public static function createModel($postData)
    {
        $model = new self();
        $model->load($postData);
        $model->save();

        return $model->id;
    }

    public static function updateModel($id, $postData)
    {
        $model = self::getModel($id);
        $model->load($postData);
        $model->save();

        return $model->id;
    }

    public static function release($id)
    {
        $model = self::getModel($id);
        $model->scenario = self::SCENARIO_RELEASE;
        $model->status = self::STATUS_ACTIVE;
        $model->save();
    }

    public static function deleteModel($id)
    {
        $model = self::getModel($id);
        $model->status = self::STATUS_DELETED;
        $model->save();
    }
}
