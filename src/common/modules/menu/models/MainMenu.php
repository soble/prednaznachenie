<?php

namespace common\modules\menu\models;

use common\models\MainModel;

/**
 * This is the model class for table "main_menu".
 *
 * @property int $id
 * @property string $name
 * @property string $sort
 * @property string $status
 * @property string $link
 * @property string $icon
 */
class MainMenu extends MainModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['name', 'sort', 'status', 'link', 'icon'], 'string', 'max' => 255
            ],
            ['status', 'default', 'value' => self::STATUS_NEW]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'sort' => 'Сортировка',
            'status' => 'Статус',
            'link' => 'Ссылка в главном меню',
            'icon' => 'Иконка',
        ];
    }
}
