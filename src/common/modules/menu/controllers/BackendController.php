<?php

namespace common\modules\menu\controllers;

use backend\controllers\MainController;
use backend\models\MainModel;
use common\modules\menu\models\MenuModel;

/**
 * Default controller for the `menu` module
 */
class BackendController extends MainController
{
    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', MainModel::NAME_MENU_MENU);
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $id = MenuModel::createModel(\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Создано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionView($id)
    {
        $model = MenuModel::getModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id)
    {
        MenuModel::updateModel($id,\Yii::$app->request->post());
        \Yii::$app->session->setFlash('success', 'Обновлено');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionRelease($id)
    {
        MenuModel::release($id);
        \Yii::$app->session->setFlash('success', 'Меню активировано');

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete($id)
    {
        MenuModel::deleteModel($id);
        \Yii::$app->session->setFlash('success', 'Удалено');

        return $this->redirect(['index']);
    }
}
