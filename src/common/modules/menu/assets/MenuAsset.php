<?php

namespace common\modules\menu\assets;

use yii\web\AssetBundle;

class MenuAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/menu/assets';
    public $css = [
        'css/index.css'
    ];

    public $depends = [
        'frontend\assets\AppAsset'
    ];
}
