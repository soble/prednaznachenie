<?php

/* @var $this yii\web\View */

use \yii\grid\GridView;
use \common\modules\menu\models\MenuModel;
use \yii\helpers\Html;

$this->title = 'Меню';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Основное меню
                    <small class="m-l-sm">Список основного меню на сайте</small>
                </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <?= $this->render('_createModal')?>
                <?= GridView::widget([
                    'dataProvider' => MenuModel::getMenuDataProvider(),
                    'columns' => [
                        [
                            'attribute' => 'name',
                            'label' => 'Наименование',
                            'content' => function ($data) {
                                return Html::a($data->name, ['/menu/backend/view', 'id' => $data->id]);
                            }
                        ],
                        [
                            'attribute' => 'link',
                            'label' => 'Ссылка',
                            'content' => function ($data) {
                                return $data->link;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'label' => 'Статус',
                            'content' => function ($data) {
                                return $data->statusHtml();
                            }
                        ],
                        [
                            'label' => '#',
                            'content' => function ($data) {
                                return Html::a(
                                        '<i class="fa fa-trash"></i>',
                                        ['/menu/backend/delete', 'id' => $data->id]
                                );
                            }
                        ],
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
