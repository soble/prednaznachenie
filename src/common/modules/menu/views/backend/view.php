<?php

/* @var $this yii\web\View */
/* @var $model MenuModel */

use \common\modules\menu\models\MenuModel;
use yii\widgets\ActiveForm;
use \yii\helpers\Url;

$this->title = $model->name;
$this->params['breadcrumbs'][] = [
    'label' => 'Меню',
    'url' => ['/menu/backend/index']
];$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $model->name ?></h5>
                <?= $model->statusHtml() ?>
                <div class="pull-right">
                    <button class="btn btn btn-warning" type="submit" form="update-form-menu">
                        <i class="fa fa-floppy-o"></i> Сохранить
                    </button>
                    <a class="btn btn-primary" href="<?= Url::to(['/menu/backend/release', 'id' => $model->id])?>">
                        Активировать
                    </a>
                    <a class="btn btn-danger" href="<?= Url::to(['/menu/backend/delete', 'id' => $model->id])?>">
                        Удалить
                    </a>
                </div>
            </div>
            <div class="ibox-content icons-box">
                <div class="row">
                    <div class="col-md-12">
                        <?php $form = ActiveForm::begin([
                            'action' => ['/menu/backend/update', 'id' => $model->id],
                            'method' => 'POST',
                            'id' => 'update-form-menu'
                        ]); ?>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <?= $form->field($model, 'sort')->textInput(['autofocus' => true]) ?>
                        </div>
                        <?php $form = ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
