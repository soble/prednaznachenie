<?php

namespace common\modules\menu\traits;

use yii\helpers\Html;

trait HtmlFront
{
    public static function menuList($menuList)
    {
        $menuStr = '<ul class="nav navbar-top-links top-menu hidden-xs" style="text-align: center">';
        $currentMenu = \Yii::$app->session->get('menu-active');
        foreach ($menuList as $menu) {
            $menuStr .= '<li class="' . ($currentMenu == $menu['link'] ? 'active' : '') . '">' .
                Html::a($menu['name'], [$menu['link']])
                . '</li>';
        }

        return $menuStr . '</ul>';
    }

    public static function mobileMenu($menuList)
    {
        $menuStr = '<ul class="nav top-menu hidden-sm hidden-md hidden-lg" style="text-align: center">';
        $currentMenu = \Yii::$app->session->get('menu-active');
        foreach ($menuList as $menu) {
            $menuStr .= '<li class="' . ($currentMenu == $menu['link'] ? 'active' : '') . '">' .
                Html::a($menu['name'], [$menu['link']])
                . '</li>';
        }

        return $menuStr . '</ul>';
    }
}