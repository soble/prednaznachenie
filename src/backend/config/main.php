<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'menu' => [
            'class' => 'common\modules\menu\Module',
        ],
        'theory' => [
            'class' => 'common\modules\theory\Module',
        ],
        'methods' => [
            'class' => 'common\modules\methods\Module',
        ],
        'tags' => [
            'class' => 'common\modules\tags\Module',
        ],
        'theory-types' => [
            'class' => 'common\modules\theoryTypes\Module',
        ],
        'reviews' => [
            'class' => 'common\modules\reviews\Module',
        ],
        'seo' => [
            'class' => 'common\modules\seo\Module',
        ],
        'description' => [
            'class' => 'common\modules\description\Module',
        ]
    ],
    'components' => [
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV3' => '6LeTObEUAAAAAMwnlAnd-4Jfd5C2qEeDrnKs5MiI',
            'secretV3' => '6LeTObEUAAAAAE_V6Yg5MBwyyDNS_m8mSkrbN8t6'
        ],
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
    ],
    'params' => $params,
    'defaultRoute' => 'menu/backend/index'
];
