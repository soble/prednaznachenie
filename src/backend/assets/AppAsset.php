<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/app';
    public $css = [
        'css/style.min.css',
        'css/main.css',
        'css/animate.css',
        'font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
        'js/jquery-ui.custom.min.js',
        'js/jquery.metisMenu.js',
        'js/jquery.slimscroll.min.js',
        'js/inspinia.js',
        'js/pace.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
