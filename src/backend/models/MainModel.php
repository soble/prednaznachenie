<?php

namespace backend\models;

use common\models\AboutProject;
use yii\base\Model;
use yii\helpers\Url;

class MainModel extends Model
{
    const NAME_MENU_ABOUT_PROJECT = 'about';
    const NAME_MENU_MENU = 'menu';
    const NAME_MENU_THEORY = 'theory';
    const NAME_MENU_METHODS = 'methods';
    const NAME_MENU_TAGS = 'tags';
    const NAME_MENU_THEORY_TYPES = 'theory_types';
    const NAME_MENU_REVIEWS = 'reviews';

    public static function menu()
    {
        if (!$aboutProject = AboutProject::find()->one()) {
            $aboutProject = new AboutProject();
        }
        $activeMenu = \Yii::$app->session->get('menu-active')
            ? \Yii::$app->session->get('menu-active')
            : self::NAME_MENU_MENU;
        return [
            'О проекте' => [
                'icon' => '<i class="fa fa-info"></i>',
                'status' => $activeMenu == self::NAME_MENU_ABOUT_PROJECT ? 'active' : '',
                'url' => Url::to(['/site/about', 'id' => $aboutProject->id])
            ],
            'Меню' => [
                'icon' => '<i class="fa fa-bars"></i>',
                'status' => $activeMenu == self::NAME_MENU_MENU ? 'active' : '',
                'url' => Url::to(['/menu/backend/index'])
            ],
            'Типизация' => [
                'icon' => '<i class="fa fa-book"></i>',
                'status' => $activeMenu == self::NAME_MENU_THEORY ? 'active' : '',
                'url' => Url::to(['/theory/backend/index'])
            ],
            'Методы' => [
                'icon' => '<i class="fa fa-dot-circle-o"></i>',
                'status' => $activeMenu == self::NAME_MENU_METHODS ? 'active' : '',
                'url' => Url::to(['/methods/backend/index'])
            ],
            'Теги' => [
                'icon' => '<i class="fa fa-tags"></i>',
                'status' => $activeMenu == self::NAME_MENU_TAGS ? 'active' : '',
                'url' => Url::to(['/tags/backend/index'])
            ],
            'Сообщения' => [
                'icon' => '<i class="fa fa-comment"></i>',
                'status' => $activeMenu == self::NAME_MENU_REVIEWS ? 'active' : '',
                'url' => Url::to(['/reviews/backend/index'])
            ],
//            'Типы теории' => [
//                'icon' => '<i class="fa fa-dot-circle-o"></i>',
//                'status' => $activeMenu == self::NAME_MENU_THEORY_TYPES ? 'active' : '',
//                'url' => Url::to(['/theory-types/backend/index'])
//            ]
        ];
    }
}
