<?php

/* @var $this yii\web\View */

/* @var $model AboutProject */

use common\modules\seo\models\SeoModel;
use common\widgets\seo\SeoWidget;
use yii\bootstrap\Tabs;
use \common\models\AboutProject;

$this->title = 'О проекте';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= $this->title ?></h5>
            </div>
            <div class="ibox-content icons-box">
                <?= Tabs::widget([
                    'items' => [
                        [
                            'label' => 'Основное',
                            'content' => $this->render('_aboutForm', [
                                    'model' => $model
                            ]),
                            'active' => true
                        ],
                        [
                            'label' => 'SEO',
                            'content' => SeoWidget::widget(['pageName' => SeoModel::PAGE_ABOUT]),
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
