<?php

/* @var $this yii\web\View */

/* @var $model AboutProject */

use common\models\Files;
use common\widgets\files\FilesWidget;
use yii\helpers\Url;
use yii\widgets\ActiveForm,
    \mihaildev\ckeditor\CKEditor,
    \common\models\AboutProject;

?>

<div class="row" style="margin-top: 20px">
    <div class="col-md-12">
        <div class="pull-right">
            <button class="btn btn btn-warning" type="submit" form="update-form-about">
                <i class="fa fa-floppy-o"></i> Сохранить
            </button>
        </div>
        <?php $form = ActiveForm::begin([
            'action' => ['/site/update-about', 'id' => $model->id],
            'method' => 'POST',
            'id' => 'update-form-about'
        ]); ?>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?= $form->field($model, 'description')->widget(CKEditor::class, [
                'editorOptions' => [
                    'preset' => 'full',
                    'inline' => false,
                ],
            ]); ?>
        </div>
        <?php $form = ActiveForm::end(); ?>
    </div>
    <?= FilesWidget::widget([
        'type' => Files::TYPE_ABOUT,
        'modelId' => $model->id,
        'urlForCreate' => Url::to(['/files/upload']),
        'urlForDelete' => '/files/delete'
    ])?>
</div>
