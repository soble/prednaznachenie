<?php

use yii\helpers\Url;
use \backend\models\MainModel;

?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a href="<?= Url::to('/') ?>">
                        <strong class="font-bold">Предназначение</strong>
                    </a>
                </div>
                <div class="logo-element">
                    П-Е
                </div>
            </li>
            <?php foreach (MainModel::menu() as $menuName => $data) : ?>
                <li class="<?= $data['status'] ?>">
                    <a href="<?= $data['url'] ?>">
                        <?= $data['icon'] ?>
                        <span class="nav-label"><?= $menuName ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>