<?php

use \yii\widgets\ActiveForm;
use \yii\helpers\Url;
use \yii\helpers\Html;

?>
<nav class="navbar white-bg" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
        </a>
        <?php $form = ActiveForm::begin([
            'method' => 'post',
            'id' => 'search-form',
            'action' => Url::to(['site/search']),
            'options' => [
                'class' => 'navbar-form-custom'
            ]
        ]) ?>
        <div class="form-group">
            <input type="text" placeholder="Поиск..." class="form-control" name="top-search"
                   id="top-search">
        </div>
        <?php ActiveForm::end() ?>
    </div>
    <?php if (!Yii::$app->user->isGuest) {
        echo Html::a('<i class="fa fa-sign-out"></i> Log out', Url::to(['/site/logout']), [
                'data-method' => 'POST',
                'class' => 'pull-right btn btn-white',
                'style' => 'margin: 10px 20px'
        ]);
    }?>
</nav>