<?php

namespace backend\controllers;

use common\traits\ErrorTrait;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class MainController extends Controller
{
    use ErrorTrait;

    const ERROR_CODE_FOR_ADMIN = 666;
    const ERROR_CODE_FOR_USER_AND_ADMIN = 766;
    const NOTIFY_USER_AND_ADMIN = 100;
    const NOTIFY_CODE_FOR_USER = 110;
    const EMPTY_CODE = 0;
    const PHP_ERROR_CODE = 2;

    public function init()
    {
        date_default_timezone_set('Europe/Moscow');
        Yii::$app->language = 'ru-RU';
        mb_internal_encoding('utf-8');
        setlocale(LC_COLLATE, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');
        parent::init();
    }

    public function beforeAction($action)
    {
        $actionName = $action->id;
        if (!in_array($actionName, ['login'])) {
            if (Yii::$app->user->isGuest) {
                $this->redirect(['/site/login']);
                return false;
            }
        }

        return parent::beforeAction($action);
    }

    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (\Throwable $e) {
            return $this->buildError($e, $id);
        }
    }
}
