<?php
namespace backend\controllers;

use common\models\Files;
use Yii;

/**
 * Site controller
 */
class FilesController extends MainController
{
    public function actionUpload()
    {
        $model = new Files();
        $model->load(Yii::$app->request->post());
        if ($model->upload()) {
            Yii::$app->session->setFlash('success', 'Фаил загружен');
        } else {
            Yii::$app->session->setFlash('error', 'Фаил не загружен');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete($id)
    {
        Files::deleteFile($id);
        Yii::$app->session->setFlash('success', 'Фаил удалён');

        return $this->redirect(Yii::$app->request->referrer);
    }
}
