<?php
$stringToConnect = 'pgsql:host=' . getenv('DB_HOST')
    . ';port=' . getenv('DB_PORT')
    . ';dbname=' . getenv('DB_MISSION_NAME');
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => $stringToConnect,
            'username' => getenv('POSTGRES_USER'),
            'password' => getenv('POSTGRES_PASSWORD'),
            'charset' => 'utf8'
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
