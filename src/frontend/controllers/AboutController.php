<?php

namespace frontend\controllers;

use common\models\AboutProject;
use common\modules\seo\models\SeoModel;
use common\widgets\seo\SeoWidget;

/**
 * Site controller
 */
class AboutController extends MainController
{
    const PAGE_SITE_ABOUT = '/';

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        \Yii::$app->session->set('menu-active', self::PAGE_SITE_ABOUT);
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        \Yii::$app->session->set('menu-active', self::PAGE_SITE_ABOUT);
        if (!$model = AboutProject::find()->one()) {
            $model = new AboutProject();
        }
        SeoWidget::registerStaticPageMetaTags(SeoModel::PAGE_ABOUT, 'О проекте Предназначение.help');

        return $this->render('about', [
            'model' => $model
        ]);
    }
}
