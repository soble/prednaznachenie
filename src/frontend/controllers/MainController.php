<?php

namespace frontend\controllers;

use common\modules\menu\models\MenuModel;
use common\traits\ErrorTrait;
use Yii;
use yii\web\Controller;

/**
 * Site controller
 */
class MainController extends Controller
{
    use ErrorTrait;

    const ERROR_CODE_FOR_ADMIN = 666;
    const ERROR_CODE_FOR_USER_AND_ADMIN = 766;
    const NOTIFY_USER_AND_ADMIN = 100;
    const NOTIFY_CODE_FOR_USER = 110;
    const EMPTY_CODE = 0;
    const PHP_ERROR_CODE = 2;

    public $leftBarContent = '';
    public $leftMenuItems = '';
    public $leftMenuTitle = '';

    public function init()
    {
        date_default_timezone_set('Europe/Moscow');
        Yii::$app->language = 'ru-RU';
        mb_internal_encoding('utf-8');
        setlocale(LC_COLLATE, 'ru_RU.UTF-8');
        setlocale(LC_CTYPE, 'ru_RU.UTF-8');
        $this->view->params['leftBarContent'] = $this->leftBarContent;
        $this->view->params['leftMenuItems'] = $this->leftMenuItems;
        $this->view->params['leftMenuTitle'] = $this->leftMenuTitle;
        $this->view->params['menuList'] = MenuModel::find()
            ->where(['status' => MenuModel::STATUS_ACTIVE])
            ->asArray()
            ->orderBy('sort')
            ->all();
        parent::init();
    }

    public function runAction($id, $params = [])
    {
        try {
            return parent::runAction($id, $params);
        } catch (\Throwable $e) {
            if (isset($e->statusCode) && in_array($e->statusCode, [400, 405])) {
                return parent::runAction($id, $params);
            }
            return $this->buildError($e, $id);

        }
    }
}
