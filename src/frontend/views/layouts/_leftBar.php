<?php

/* @var $leftMenuItems string */
/* @var $leftMenuTitle string */

?>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav">
            <li class="nav-header">
                <div class="dropdown profile-element" style="color: #DFE4ED; text-align: center">
                    <?= $leftMenuTitle ?>
                </div>
            </li>
            <?= $leftMenuItems ?>
        </ul>
    </div>
</nav>
