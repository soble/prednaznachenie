<?php

use himiklab\yii2\recaptcha\ReCaptcha3,
    \yii\widgets\ActiveForm,
    \yii\helpers\Url,
    frontend\models\SearchModel;

$model = new SearchModel();

$form = ActiveForm::begin([
    'method' => 'get',
    'id' => 'search-form',
    'action' => Url::to(['/site/search']),
    'options' => [
        'class' => 'navbar-form-custom'
    ]
]);
echo $form->field($model, 'captcha')->widget(
    ReCaptcha3::class,
    ['action' => 'search']
)->label(false);

echo '<input type="text" placeholder="Поиск..." class="form-control autofocus-field-search" name="SearchModel[searchText]" autofocus>';
ActiveForm::end();
