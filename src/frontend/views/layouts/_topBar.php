<?php
/**
 * @var $menuList MenuModel[]
 */

use \common\modules\menu\models\MenuModel,
    \common\modules\menu\assets\MenuAsset;

MenuAsset::register($this);
?>
<nav id="topNav" class="navbar navbar-fixed-top white-bg" role="navigation" style="margin-bottom: 0;">
    <div class="search-box over-header">
        <a id="closeSearch" href="#" class="fa fa-remove"></a>
        <?= $this->render('__searchForm') ?>
    </div>
    <div class="float-header-button" style="margin-right: 10px;">
        <div class="pull-right" style="padding: 15px 10px;">
            <?= $this->render('@common/modules/reviews/views/frontend/_errorModalNavBar') ?>
        </div>
        <a class="pull-right search">
            <i class="fa fa-search"></i>
        </a>
    </div>
    <div class="pull-left hidden-xs" style="margin-right: 10px; width: 200px; opacity: 0">
        help block
    </div>
    <a class="show-mobile-menu minimalize-styl-2 btn btn-primary pull-right hidden-lg hidden-md hidden-sm" href="#">
        <i class="fa fa-bars"></i>
    </a>
    <?= MenuModel::menuList($menuList) ?>
    <div class="mobile-menu white-bg">
        <?= MenuModel::mobileMenu($menuList) ?>
    </div>
</nav>
