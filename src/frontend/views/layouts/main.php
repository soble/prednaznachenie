<?php

/* @var $this \yii\web\View */

/* @var $content string */
/* @var $leftBarContent string */

use frontend\widgets\statistics\StatisticsWidget;
use yii\helpers\Html,
    frontend\assets\AppAsset,
    common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="boxed-layout fixed-nav">
<?php $this->beginBody() ?>
<?= StatisticsWidget::widget() ?>
<div id="wrapper">

    <?= $this->render('_leftBar', [
            'leftMenuItems' => $this->params['leftMenuItems'],
            'leftMenuTitle' => $this->params['leftMenuTitle'],
    ]) ?>
    <div id="page-wrapper" class="gray-bg">
        <?= $this->render('_topBar', [
                'menuList' => $this->params['menuList']
        ]) ?>
        <div class="wrapper wrapper-content animated fadeInRight">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="footer">
            <?= $this->render('_footer') ?>
        </div>

    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
