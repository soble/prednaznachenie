<?php

?>
<div>
    <?= $this->render('@common/modules/reviews/views/frontend/_createModalFooter') ?>
</div>
<div>
    <strong><?= strtotime('2019-12-31 24:59:59') < time() ? '2019-' . date('Y') : date('Y')?>
        &copy; Prednaznachenie.help</strong>
    <p class="hidden-xs">
        Дорогие друзья, если вам понравились какие-то статьи, и вы хотите разместить их у себя на сайте или в блоге – копируйте на здоровье! Только пожалуйста укажите ссылку на этот ресурс – буду очень вам благодарен!
    </p>
    <p style="font-size: 10px;color: gray;">This site is protected by reCAPTCHA and the Google
        <a href="https://policies.google.com/privacy">Privacy Policy</a>and
        <a href="https://policies.google.com/terms">Terms of Service</a> apply.
    </p>
</div>
