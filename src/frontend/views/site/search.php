<?php

/* @var $this yii\web\View */
/* @var $model \frontend\models\SearchModel */
/* @var $data ArrayDataProvider */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use \frontend\models\SearchModel;

$this->title = 'Поиск';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
            <div class="site-about ibox-content">
                <div class="row">
                    <div class="col-xs-12 col-md-12" style="padding: 10px 15px">
                        <b>Текст поиска:</b> <?= $model->searchText ?>
                    </div>
                    <div class="col-xs-12 col-md-12" id="table-data-search">
                        <?= GridView::widget([
                            'dataProvider' => $data,
                            'showHeader' => false,
                            'tableOptions' => ['class' => 'table table-hover'],
                            'summary' => false,
                            'rowOptions' => function ($model, $key, $index, $grid) {
                                if (isset($model->tagsIndexByName)) {
                                    $url = Url::to(['/methods/frontend/view', 'methodName' => $model->getNameForUrl()]);
                                } else {
                                    $url = Url::to(['/theory/frontend/theory-by-search', 'id' => $model->id]);
                                }

                                return [
                                    'data-url' => $url,
                                    'class' => 'view-method-row'
                                ];
                            },
                            'columns' => [
                                [
                                    'content' => function ($data) use ($model) {
                                        $text = SearchModel::getTextBySearch($data->description, $model->searchText);
                                        if (isset($data->tagsIndexByName)) {
                                            $content = '<b>'
                                                . SearchModel::getTextBySearch($data->name, $model->searchText, true)
                                                . '</b>'
                                                . ' (Методы: ' . (implode(' / ', array_keys($data->tagsIndexByName))) . ') '
                                                . '<p>'
                                                . $text
                                                . '</p>';
                                        } else {
                                            $content = '<b>'
                                                . SearchModel::getTextBySearch($data->name, $model->searchText, true)
                                                . '</b> (Типизация: '
                                                . (implode(' / ', array_keys($data->parentTheories)))
                                                .')'. '<p>' . $text . '</p>';
                                        }

                                        return $content;
                                    }
                                ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
