<?php

/* @var $this yii\web\View */
/* @var $model AboutProject */

use yii\helpers\Html,
    \common\models\AboutProject;

$this->params['breadcrumbs'][] = 'О проекте Предназначение.help';
?>
<div class="row">
    <div class="col-xs-12 col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1><?= Html::encode('О проекте Предназначение.help') ?></h1>
            </div>
            <div class="site-about ibox-content">
                <?= $model->description ?>
            </div>
        </div>
    </div>
</div>
