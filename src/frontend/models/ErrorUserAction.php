<?php

namespace frontend\models;

use yii\base\Exception;
use yii\base\UserException;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;

class ErrorUserAction extends ErrorAction
{
    protected function getExceptionName()
    {
        if ($this->exception instanceof Exception) {
            $name = $this->exception->getName();
        } else {
            $name = $this->defaultName;
        }

        if ($code = $this->getExceptionCode()) {
            $name .= " (#$code)";
        }
        if ($this->exception instanceof NotFoundHttpException) {
            $name = 'Ошибка 404. Такой страницы не существует';
        }

        return $name;
    }

    protected function getExceptionMessage()
    {
        $message = $this->defaultMessage;
        if ($this->exception instanceof UserException) {
            $message = $this->exception->getMessage();
        }
        if ($this->exception instanceof NotFoundHttpException) {
            $message = 'Page not found (#404)';
        }

        return $message;
    }
}
