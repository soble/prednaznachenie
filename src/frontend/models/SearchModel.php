<?php

namespace frontend\models;

use common\modules\methods\models\MethodsSearch;
use common\modules\theory\models\TheorySearch;
use frontend\controllers\MainController;
use himiklab\yii2\recaptcha\ReCaptchaValidator3;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class SearchModel extends Model
{
    public $captcha;
    public $searchText;

    public function rules()
    {
        return [
            ['searchText', 'string', 'max' => 100],
            ['searchText', 'required'],
            [['captcha'], ReCaptchaValidator3::class,
                'threshold' => 0.1,
                'action' => 'search',
                'message' => \Yii::$app->params['errorMessageCaptcha']]
        ];
    }

    public static function getTextBySearch($text, $searchText, $isName = false)
    {
        $text = strip_tags($text);
        $position = mb_stripos($text, $searchText);
        if ($position === false) {
            $text = mb_substr($text, 0, 200);
        } else {
            $beginPositionInText = ($position - 40) < 0 ? 0 : ($position - 40);
            $text = mb_substr($text, $beginPositionInText, 200);
        }

        $patterns = "/(" . $searchText . ")+/ui";
        $replace = "<span class=\"select-search\">$1</span>";
        $returnText = preg_replace($patterns, $replace, $text);

        return $isName ? $returnText : '...' . $returnText . '...';
    }

    public function search()
    {
        if (!$this->searchText) {
            throw new \Exception('Введите текст для поиска', MainController::NOTIFY_CODE_FOR_USER);
        }
        $methods = MethodsSearch::getMethodsForSearch($this->searchText);
        $theory = TheorySearch::getTheoriesForSearch($this->searchText);

        return new ArrayDataProvider([
            'allModels' => array_merge($methods, $theory),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }
}
