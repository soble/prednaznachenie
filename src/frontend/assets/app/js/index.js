$(document).ready(function () {
    $(document).on('click', '.show-mobile-menu', showMobileMenu);
    $(document).on('click', '.show-mobile-settings-theory', showMobileSettingsTheory);
    $(document).on('click', '.view-method-row', viewMethodRow);
    $(document).on('click', '.search', actionShowSearch);
    $(document).on('click', '#closeSearch', actionShowSearch);
});

function showMobileMenu(e) {
    e.preventDefault();
    let menu = $('.mobile-menu');

    if (menu.is(':visible')) {
        menu.slideUp();
    } else {
        menu.slideDown();
    }
}

function showMobileSettingsTheory(e) {
    e.preventDefault();
    let menu = $('.mobile-settings-methods'),
        page = $('#page-wrapper'),
        content = $('.ibox-content');
    if (menu.is(':visible')) {
        menu.slideUp(300, function () {
        });
        page.css({'padding': '0 15px'});
    } else {
        menu.slideDown(300, function () {
            let heightMenu = menu.height();
            let heightContent = content.height();
            console.log('heightMenu', heightMenu);
            console.log('heightContent', heightContent);
            if (heightMenu > heightContent) {
                content.height(heightMenu - 30);
            }
        });
        page.css({'padding': '0'});
    }
}

function viewMethodRow() {
    window.location.href = $(this).data('url');
}

function actionShowSearch(e) {
    e.preventDefault();
    $('.search-box').toggle();
    $(".autofocus-field-search").focus();
}
