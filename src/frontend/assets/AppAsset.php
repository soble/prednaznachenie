<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/app';
    public $css = [
        'css/style.min.css',
        'css/animate.css',
        'font-awesome/css/font-awesome.min.css',
        'css/index.css',
    ];
    public $js = [
        'js/jquery-ui.custom.min.js',
        'js/jquery.metisMenu.js',
        'js/jquery.slimscroll.min.js',
        'js/inspinia.js',
        'js/pace.min.js',
        'js/index.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
