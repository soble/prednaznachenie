<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/methods' => '/methods/frontend/index',
                '/' => '/about/index',
                '' => '/about/index',
                'order' => '/methods/frontend/order',
                'site-map' => '/seo/frontend-seo/site-map',
                'search' => '/site/search',
                'feedback' => '/reviews/frontend',
                'types' => '/theory/frontend',
                'methods-set-tag/<tag>' => '/methods/frontend/set-tag',
                'methods/<methodName>' => '/methods/frontend/view',
//                'types-detail/<parentTheory>' => '/theory/frontend/child-theories',
//                'types/<name>/<id:\d+>/<parentTheory:\d+>' => '/theory/frontend/view',
//                'types/<parentTheoryName>/<theoryName>' => '/theory/frontend/view',
                [
                    'pattern' => '<controller>/<action>',
                    'route' => '<controller>/<action>',
                    'suffix' => ''
                ],
            ]
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV3' => '6LeTObEUAAAAAMwnlAnd-4Jfd5C2qEeDrnKs5MiI',
            'secretV3' => '6LeTObEUAAAAAE_V6Yg5MBwyyDNS_m8mSkrbN8t6',
            'httpClient' => 'yii\httpclient\Client'
        ],
    ],
    'defaultRoute' => '/methods/frontend',
    'params' => $params,
    'modules' => [
        'menu' => [
            'class' => 'common\modules\menu\Module',
        ],
        'theory' => [
            'class' => 'common\modules\theory\Module',
        ],
        'methods' => [
            'class' => 'common\modules\methods\Module',
        ],
        'reviews' => [
            'class' => 'common\modules\reviews\Module',
        ],
        'seo' => [
            'class' => 'common\modules\seo\Module',
        ]
    ],
];
