<?php

namespace frontend\widgets\statistics;

use yii\base\Widget;
use yii\web\View;

class StatisticsWidget extends Widget
{
    const YANDEX_VERIFICATION_TAG = 'yandex-verification';
    const YANDEX_VERIFICATION_CONTENT = '74cda0bbe4999b86';

    public function run()
    {
        $this->yandex();
        $this->google();
    }

    public function yandex()
    {
        $this->view->registerJs('
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
            
            ym(56228167, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
            });', View::POS_BEGIN);

        $this->view->registerMetaTag([
            'name' => self::YANDEX_VERIFICATION_TAG,
            'content' => self::YANDEX_VERIFICATION_CONTENT
        ], self::YANDEX_VERIFICATION_TAG);

        echo '<noscript><div>
            <img src="https://mc.yandex.ru/watch/56228167" style="position:absolute; left:-9999px;" alt="" /></div>
            </noscript>';
    }

    public function google()
    {
        $this->view->registerJsFile('https://www.googletagmanager.com/gtag/js?id=UA-152555151-1',
            ['position' => View::POS_HEAD, 'async' => true]);
        $this->view->registerJs("window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-152555151-1');", View::POS_BEGIN
        );
    }
}
