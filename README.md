## Overview
This is a Dockerfile/image to build a container for nginx and php-fpm

### Versioning
| Docker Tag | Git Release | Nginx Version | PHP Version | Alpine Version |
|-----|-------|-----|--------|--------|
| latest | Master Branch |1.16.1 | 7.4.5 | 3.11 |
