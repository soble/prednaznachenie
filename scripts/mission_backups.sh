#!/bin/sh

# mjr

BACKUPDIR=/backups/mission
TEMPDIR=/backups/tmp
PGSQLBINDIR='docker exec pgsql'
DBNAME='mission'

CUR_DAY=$(date +%d)
CUR_DAYOFWEEK=$(date +%w)
CUR_WEEK=$(date +%V)
CUR_MONTH=$(date +%m)
CUR_YEAR=$(date +%Y)

#PREFIX="daily"
#PREFIX_DATE=$CUR_DAY

PREFIX="weekly"
PREFIX_DATE=$CUR_WEEK

if test $CUR_DAY = "01"; then
  if test $CUR_MONTH = "01"; then
    PREFIX="yearly"
    PREFIX_DATE=$CUR_YEAR
  else
    PREFIX="monthly"
    PREFIX_DATE=$CUR_MONTH
  fi
fi

ARCHPREFIX=$PREFIX"_"$PREFIX_DATE"_pg"

echo "================================================================================"
echo "Starting backup"
echo $(date '+%m.%d.%Y %H:%M:%S')

cd $TEMPDIR

echo "dumping database: $DBNAME ..."
# schema
echo "dumping schema ..."
$PGSQLBINDIR pg_dump -f /tmp/$DBNAME"_schema".bak -C -s -U reactor -p 5432 $DBNAME
echo "done"
filewoext=$DBNAME"_schema"
bakfile=/tmp/$filewoext".bak"
archivename=$BACKUPDIR/$ARCHPREFIX"_"$filewoext.tar.gz
archivenametmp=$TEMPDIR/$ARCHPREFIX"_"$filewoext.tar.gz
if test -e $archivename; then
  echo "removing old archive ..."
  rm -f $archivename
  echo "done"
fi
echo "archiving schema ..."
$PGSQLBINDIR tar cfz $archivenametmp $bakfile
$PGSQLBINDIR mv -f $archivenametmp $archivename
echo "done"
# data
echo "removing file: $bakfile ..."
rm -f $bakfile
echo "done"
echo "dumping data ..."
$PGSQLBINDIR pg_dump -f $DBNAME"_data".bak -a --disable-triggers -U reactor -p 5432 $DBNAME
echo "done"
filewoext=$DBNAME"_data"
bakfile=$filewoext".bak"
archivename=$BACKUPDIR/$ARCHPREFIX"_"$filewoext.tar.gz
archivenametmp=$TEMPDIR/$ARCHPREFIX"_"$filewoext.tar.gz
if test -e $archivename; then
  echo "removing old archive ..."
  rm -f $archivename
  echo "done"
fi
echo "archiving data ..."
$PGSQLBINDIR tar cfz $archivenametmp $bakfile
$PGSQLBINDIR mv -f $archivenametmp $archivename
echo "done"
echo "removing file: $bakfile ..."
rm -f $bakfile
echo "done"

# Removing dumps
echo "performing post-operations ..."

cd /tmp
if cd $BACKUPDIR; then
  chown backup:users *
fi

echo $(date '+%m.%d.%Y %H:%M:%S')
echo "Finished backup"
echo "================================================================================"
echo ""
